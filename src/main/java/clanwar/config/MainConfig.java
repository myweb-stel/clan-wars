package clanwar.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mysql.jdbc.Driver;

@Configuration
@ComponentScan(basePackages = "clanwar", excludeFilters = { @Filter(Configuration.class) })
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class MainConfig {
	
	@Autowired
	private Environment env;
	
	private final String PACKAGES = "hibernate.packages";
	
	// JDBC_NAME not used with this DataSource
	//private final String JDBC_NAME = "jdbc.name";
	private final String JDBC_URL = "jdbc.url";
	private final String JDBC_USER = "jdbc.user";
	private final String JDBC_PASSWORD = "jdbc.password";
	
	private final String HIB_FORMAT_SQL = "hibernate.format_sql";
	private final String HIB_SHOW_SQL = "hibernate.show_sql";
	private final String HIB_DIALECT = "hibernate.dialect";
	
	Logger logger = Logger.getLogger(MainConfig.class);
	
	@Bean
	public SessionFactory sessionFactoy() {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
		builder.scanPackages(env.getProperty(PACKAGES));
		builder.addProperties(properties());
		SessionFactory session = builder.buildSessionFactory();
		return session;
	}
	
	@Bean
	public DataSource dataSource() {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriver(driver());
		dataSource.setUrl(env.getProperty(JDBC_URL));
		dataSource.setUsername(env.getProperty(JDBC_USER));
		dataSource.setPassword(env.getProperty(JDBC_PASSWORD));
		return dataSource;
	}
	
	@Bean(name = "transactionManager")
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactoy());
		return transactionManager;
	}

	@Bean
	public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	private Properties properties() {
		Properties properties = new Properties();
		properties.put(HIB_FORMAT_SQL, env.getProperty(HIB_FORMAT_SQL));
		properties.put(HIB_SHOW_SQL, env.getProperty(HIB_SHOW_SQL));
		properties.put(HIB_DIALECT, env.getProperty(HIB_DIALECT));
		return properties;
	}
	
	private Driver driver() {
		Driver driver = null;
		try {
			driver = new com.mysql.jdbc.Driver();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return driver;
	}

}
