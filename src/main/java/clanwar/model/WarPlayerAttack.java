package clanwar.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "WAR_PLAYERS_ATTACKS")
public class WarPlayerAttack implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "WAR_PLAYER")
	private WarPlayer warPlayer;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "ATTACK")
	private Attack attack;
	
	@ManyToOne
	@JoinColumn(name = "TYPE_ATTACK")
	private TypeAttack typeAttackInWar;
	
	public WarPlayerAttack() { }

	@JsonBackReference
	public WarPlayer getWarPlayer() {
		return warPlayer;
	}

	public void setWarPlayer(WarPlayer warPlayer) {
		this.warPlayer = warPlayer;
	}

	public Attack getAttack() {
		return attack;
	}

	public void setAttack(Attack attack) {
		this.attack = attack;
	}

	public TypeAttack getTypeAttackInWar() {
		return typeAttackInWar;
	}

	public void setTypeAttackInWar(TypeAttack typeAttackInWar) {
		this.typeAttackInWar = typeAttackInWar;
	}
	
}
