package clanwar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "WAR_ENEMIES")
public class WarEnemy {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "WAR")
	private War war;
	
	@Column(name = "DIFFCULTY")
	private int diffculty;

	@Column(name = "WAR_NUMBER")
	private int warNumber;
	
	public WarEnemy() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonBackReference
	public War getWar() {
		return war;
	}

	public void setWar(War war) {
		this.war = war;
	}

	public int getDiffculty() {
		return diffculty;
	}

	public void setDiffculty(int diffculty) {
		this.diffculty = diffculty;
	}

	public int getWarNumber() {
		return warNumber;
	}

	public void setWarNumber(int warNumber) {
		this.warNumber = warNumber;
	}
	
}
