package clanwar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

// TODO add trops for castle clan

@Entity
@Table(name = "ATTACKS")
public class Attack implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "ENEMY")
	private WarEnemy enemy;
	
	@Column(name = "STARS")
	private int stars;
	
	@ManyToOne
	@JoinColumn(name = "STRATEGY")
	private Strategy strategy;
	
	@ManyToOne
	@JoinColumn(name = "TIME_RANGE")
	private TimeRange timeRange;
	
	// TODO attackPercentaje
	
	public Attack() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@JsonManagedReference
	public WarEnemy getEnemy() {
		return enemy;
	}
	
	public void setEnemy(WarEnemy enemy) {
		this.enemy = enemy;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public TimeRange getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(TimeRange timeRange) {
		this.timeRange = timeRange;
	}
	
}
