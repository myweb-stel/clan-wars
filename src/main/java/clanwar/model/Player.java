package clanwar.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "PLAYERS")
public class Player implements Serializable, UserDetails {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "USERNAME", nullable = false)
	private String username;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
	private Set<PlayerRole> roles;
	
	@Transient
	private Set<GrantedAuthority> authorities;
	
	@Column(name = "ACCOUNT_NON_EXPIRED")
	private boolean accountNonExpired;
	
	@Column(name = "ACCOUNT_NON_LOCKED")
	private boolean accountNonLocked;
	
	@Column(name = "CREDENTIALS_NON_EXPIRED")
	private boolean credentialsNonExpired;
	
	@Column(name = "ENABLED")
	private boolean enabled;
	
	@Column(name = "HAVE_KING")
	private boolean haveKing;
	
	@Column(name = "HAVE_QUEEN")
	private boolean haveQueen;
	
	@Column(name = "HAVE_POTIONS")
	private boolean havePotions;
	
	public Player() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@JsonManagedReference
	public Set<PlayerRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<PlayerRole> roles) {
		this.roles = roles;
	}

	@Override
	public Set<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isHaveKing() {
		return haveKing;
	}

	public void setHaveKing(boolean haveKing) {
		this.haveKing = haveKing;
	}

	public boolean isHaveQueen() {
		return haveQueen;
	}

	public void setHaveQueen(boolean haveQueen) {
		this.haveQueen = haveQueen;
	}

	public boolean isHavePotions() {
		return havePotions;
	}

	public void setHavePotions(boolean havePotions) {
		this.havePotions = havePotions;
	}

}
