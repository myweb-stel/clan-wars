package clanwar.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "WARS")
public class War implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CLAN")
	private Clan clan;
	
	@Column(name = "START_DATE")
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss")
	private Date startDate;
	
	@Column(name = "ENEMY_NAME")
	private String enemyName;
	
	@Column(name = "WAR_MODE")
	private int warMode;
	
	@OneToMany(mappedBy = "war")
	@OrderBy(value = "warNumber")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<WarPlayer> players;
	
	@OneToMany(mappedBy = "war")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<WarEnemy> enemies;
	
	@OneToMany(mappedBy = "war")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<WarImage> images;
	
	public War() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Clan getClan() {
		return clan;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getEnemyName() {
		return enemyName;
	}

	public void setEnemyName(String enemyName) {
		this.enemyName = enemyName;
	}

	public int getWarMode() {
		return warMode;
	}

	public void setWarMode(int warMode) {
		this.warMode = warMode;
	}

	@JsonManagedReference
	public List<WarPlayer> getPlayers() {
		return players;
	}

	public void setPlayers(List<WarPlayer> players) {
		this.players = players;
	}

	public List<WarEnemy> getEnemies() {
		return enemies;
	}

	public void setEnemies(List<WarEnemy> enemies) {
		this.enemies = enemies;
	}
	
}
