package clanwar.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "CLANS")
public class Clan implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NAME")
	private String name;
	
	@OneToMany(mappedBy = "clan")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ClanMember> members;
	
	@OneToMany(mappedBy = "clan")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ClanRequest> requests;

	public Clan() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonManagedReference
	public List<ClanMember> getMembers() {
		return members;
	}

	public void setMembers(List<ClanMember> members) {
		this.members = members;
	}

	@JsonManagedReference
	public List<ClanRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<ClanRequest> requests) {
		this.requests = requests;
	}

}
