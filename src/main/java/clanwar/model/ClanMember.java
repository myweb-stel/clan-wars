package clanwar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@DynamicUpdate(value = true)
@Table(name = "CLAN_MEMBERS")
public class ClanMember implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLAN")
	private Clan clan;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "MEMBER")
	private Player member;
	
	@ManyToOne
	@JoinColumn(name = "ROLE")
	private ClanRole role;
	
	@Column(name = "WAR_NUMBER")
	private int warNumber;
	
	public ClanMember() {}

	@JsonBackReference
	public Clan getClan() {
		return clan;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
	}

	public Player getMember() {
		return member;
	}

	public void setMember(Player member) {
		this.member = member;
	}

	public ClanRole getRole() {
		return role;
	}

	public void setRole(ClanRole role) {
		this.role = role;
	}

	public int getWarNumber() {
		return warNumber;
	}

	public void setWarNumber(int warNumber) {
		this.warNumber = warNumber;
	}
	
}
