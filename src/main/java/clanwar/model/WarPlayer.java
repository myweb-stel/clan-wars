package clanwar.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@DynamicUpdate(value = true)
@Table(name = "WAR_PLAYERS")
public class WarPlayer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "WAR")
	private War war;
	
	@ManyToOne
	@JoinColumn(name = "PLAYER")
	private Player player;
	
	@Column(name = "WAR_NUMBER")
	private int warNumber;
	
	@Column(name = "HAVE_KING")
	private boolean haveKing;
	
	@Column(name = "HAVE_QUEEN")
	private boolean haveQueen;
	
	@Column(name = "HAVE_POTIONS")
	private boolean havePotions;
	
	@OneToMany(mappedBy = "warPlayer", fetch = FetchType.EAGER)
	private List<WarPlayerAttack> attacks;
	
	public List<WarPlayerAttack> getAttacks() {
		return attacks;
	}

	public void setAttacks(List<WarPlayerAttack> attacks) {
		this.attacks = attacks;
	}

	private String comment;
	
	public WarPlayer() {}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonBackReference
	public War getWar() {
		return war;
	}

	public void setWar(War war) {
		this.war = war;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public int getWarNumber() {
		return warNumber;
	}
	
	public void setWarNumber(int warNumber) {
		this.warNumber = warNumber;
	}

	public boolean isHaveKing() {
		return haveKing;
	}

	public void setHaveKing(boolean haveKing) {
		this.haveKing = haveKing;
	}

	public boolean isHaveQueen() {
		return haveQueen;
	}

	public void setHaveQueen(boolean haveQueen) {
		this.haveQueen = haveQueen;
	}

	public boolean isHavePotions() {
		return havePotions;
	}

	public void setHavePotions(boolean havePotions) {
		this.havePotions = havePotions;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
