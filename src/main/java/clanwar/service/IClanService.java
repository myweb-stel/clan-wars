package clanwar.service;

import java.util.List;

import clanwar.common.exception.ServiceException;
import clanwar.model.Clan;
import clanwar.model.ClanMember;
import clanwar.model.ClanRequest;

public interface IClanService {

	void saveClan(Clan clan, String leader) throws ServiceException;
	
	void saveClanRequest(ClanRequest clanRequest, String username) throws ServiceException;
	
	void deleteClanRequest(ClanRequest clanRequest) throws ServiceException;
	
	void joinClan(ClanMember member) throws ServiceException;
	
	void leaveClan(ClanMember member) throws ServiceException;
	
	void updateWarNumber(ClanMember clanMember) throws ServiceException;
	
	ClanMember findMemberByPlayerId(int id) throws ServiceException;
	
	List<Clan> findAllClans() throws ServiceException;
	
	Clan findClanById(int id) throws ServiceException;
	
	List<Clan> findClanByName(Clan clan) throws ServiceException;
	
	Clan findClandByPlayerId(int id) throws ServiceException;
	
	Clan findClanByPlayerUsername(String username) throws ServiceException;

	boolean allowEdit(String username);
	
}
