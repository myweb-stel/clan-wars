package clanwar.service;

import java.util.List;

import clanwar.common.exception.ServiceException;
import clanwar.model.ClanRole;
import clanwar.model.PlayerRole;
import clanwar.model.Strategy;
import clanwar.model.TimeRange;
import clanwar.model.TypeAttack;

public interface ICommonService {

	List<Strategy> findAllStrategies() throws ServiceException;
	
	List<TimeRange> findAllTimesRanges() throws ServiceException;
	
	List<TypeAttack> findAllTipesAttacks() throws ServiceException;
	
	List<PlayerRole> findAllPlayerRoles() throws ServiceException;
	
	List<ClanRole> findAllClanRoles() throws ServiceException;
	
}
