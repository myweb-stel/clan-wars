package clanwar.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import clanwar.common.exception.ServiceException;
import clanwar.dao.IPlayerDao;
import clanwar.dao.IPlayerRoleDao;
import clanwar.dao.IWarPlayerDao;
import clanwar.model.Player;
import clanwar.model.PlayerRole;
import clanwar.model.War;
import clanwar.model.WarPlayer;
import clanwar.service.IPlayerService;

@Service
public class PlayerService implements IPlayerService, UserDetailsService {

	@Autowired
	private IPlayerDao playerDao;
	
	@Autowired
	private IPlayerRoleDao playerRoleDao;
	
	@Autowired
	private IWarPlayerDao warPlayerDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void savePlayer(Player player) {
		
		Player tempPlayer = playerDao.findByName(player.getUsername());
		
		if (tempPlayer != null) {
			throw new ServiceException("Ya existe un usuario con ese nombre");
		}
		
		player.setPassword(passwordEncoder.encode(player.getPassword()));
		player.setAccountNonExpired(true);
		player.setAccountNonLocked(true);
		player.setCredentialsNonExpired(true);
		player.setEnabled(true);
		playerDao.save(player);
	}
	
	@Override
	public Player findPlayerByName(String name) {
		return playerDao.findByName(name);
	}

	@Override
	public void savePlayerInWar(WarPlayer warPlayer) {
		warPlayerDao.save(warPlayer);
	}
	
	@Override
	public void updatePlayerInWar(WarPlayer warPlayer) {
		warPlayerDao.update(warPlayer);
	}

	@Override
	public List<WarPlayer> findPlayersInWar(War war) {
		return warPlayerDao.findPlayersInWar(war);
	}

	@Override
	public void savePlayerRole(PlayerRole playerRole) {
		playerRoleDao.save(playerRole);
	}
	
	
	
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		Player player = playerDao.findByName(username);
		setPlayerRoles(player);
		
		return player;
		
	}
	
	private void setPlayerRoles(Player player) {
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		
		for (PlayerRole role : player.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getRole()));
		}
		
		player.setAuthorities(authorities);
		
	}
}
