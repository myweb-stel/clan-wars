package clanwar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import clanwar.common.exception.ServiceException;
import clanwar.dao.IClanRoleDao;
import clanwar.dao.IPlayerRoleDao;
import clanwar.dao.IStrategyDao;
import clanwar.dao.ITimeRangeDao;
import clanwar.dao.ITypeAttackDao;
import clanwar.model.ClanRole;
import clanwar.model.PlayerRole;
import clanwar.model.Strategy;
import clanwar.model.TimeRange;
import clanwar.model.TypeAttack;
import clanwar.service.ICommonService;

@Service
public class CommonService implements ICommonService {

	@Autowired
	private IStrategyDao strategyDao;
	
	@Autowired
	private ITimeRangeDao timeRangeDao;
	
	@Autowired
	private ITypeAttackDao typeAttackDao;
	
	@Autowired
	private IPlayerRoleDao playerRoleDao;
	
	@Autowired
	private IClanRoleDao clanRoleDao;
	
	@Override
	public List<Strategy> findAllStrategies() throws ServiceException {
		return strategyDao.findAll();
	}
	
	@Override
	public List<TimeRange> findAllTimesRanges() throws ServiceException {
		return timeRangeDao.findAll();
	}
	
	@Override
	public List<TypeAttack> findAllTipesAttacks() throws ServiceException {
		return typeAttackDao.findAll();
	}
	
	@Override
	public List<PlayerRole> findAllPlayerRoles() throws ServiceException {
		return playerRoleDao.findAll();
	}
	
	@Override
	public List<ClanRole> findAllClanRoles() throws ServiceException {
		return clanRoleDao.findAll();
	}

}
