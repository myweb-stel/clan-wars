package clanwar.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import clanwar.common.exception.ServiceException;
import clanwar.dao.IClanDao;
import clanwar.dao.IClanMemberDao;
import clanwar.dao.IClanRequestDao;
import clanwar.dao.IClanRoleDao;
import clanwar.dao.IPlayerDao;
import clanwar.model.Clan;
import clanwar.model.ClanMember;
import clanwar.model.ClanRequest;
import clanwar.model.Player;
import clanwar.service.IClanService;

@Service
public class ClanService implements IClanService {

	@Autowired
	private IClanDao clanDao;
	
	@Autowired
	private IClanMemberDao clanMemberDao;
	
	@Autowired
	private IClanRoleDao clanRoleDao;
	
	@Autowired
	private IClanRequestDao clanRequestDao;
	
	@Autowired
	private IPlayerDao playerDao;
	
	@Override
	public void saveClan(Clan clan, String leader) throws ServiceException {
		
		Player player = playerDao.findByName(leader);
		
		ClanMember clanMember = new ClanMember();
		clanMember.setClan(clan);
		clanMember.setMember(player);
		checkMemberInClan(clanMember);
		
		if (player == null) {
			throw new ServiceException("El jugador establecido como lider no esta registrado");
		}
		
		clanDao.save(clan);
		
		ClanMember clanLeader = new ClanMember();
		
		clanLeader.setClan(clan);
		clanLeader.setMember(player);
		clanLeader.setRole(clanRoleDao.findByValue("Leader"));
		
		clanMemberDao.save(clanLeader);		
	}
	
	@Override
	public void saveClanRequest(ClanRequest clanRequest, String username) throws ServiceException {
		Player player = playerDao.findByName(username);
		clanRequest.setPlayer(player);
		clanRequest.setDateRequest(new Date());
		clanRequestDao.save(clanRequest);
	}
	
	@Override
	public void deleteClanRequest(ClanRequest clanRequest) throws ServiceException {
		clanRequestDao.delete(clanRequest);
	}

	@Override
	public void joinClan(ClanMember clanMember) throws ServiceException {
		checkMemberInClan(clanMember);
		clanMember.setRole(clanRoleDao.findByValue("Member"));
		clanMemberDao.save(clanMember);
	}

	@Override
	public void leaveClan(ClanMember member) throws ServiceException {
		clanMemberDao.delete(member);
	}
	
	@Override
	public void updateWarNumber(ClanMember clanMember) throws ServiceException {
		ClanMember foundClanMember = clanMemberDao.findByPlayerId(clanMember.getMember().getId());
		clanMember.setClan(foundClanMember.getClan());
		clanMember.setRole(foundClanMember.getRole());
		clanMemberDao.update(clanMember);
	}
	
	@Override
	public ClanMember findMemberByPlayerId(int id) throws ServiceException {
		return clanMemberDao.findByPlayerId(id);
	}
	
	@Override
	public List<Clan> findAllClans() throws ServiceException {
		return clanDao.findAll();
	}

	@Override
	public Clan findClanById(int id) throws ServiceException {
		return clanDao.findById(id);
	}
	
	@Override
	public List<Clan> findClanByName(Clan clan) throws ServiceException {
		return clanDao.findByName(clan);
	}

	@Override
	public Clan findClandByPlayerId(int id) throws ServiceException {
		return clanDao.findByPlayerId(id);
	}
	
	@Override
	public Clan findClanByPlayerUsername(String username)
			throws ServiceException {
		return clanDao.findByPlayerUsername(username);
	}
	
	@Override
	public boolean allowEdit(String username) {
		Player player = playerDao.findByName(username);
		ClanMember member = clanMemberDao.findByPlayerId(player.getId());
		
		if ("Leader".equals(member.getRole().getValue())
				|| "Co-leader".equals(member.getRole().getValue())) {
			return true;
		}
		
		return false;
	}
	
	private void checkMemberInClan(ClanMember clanMember) throws ServiceException {
		ClanMember foundMember = clanMemberDao
				.findByPlayerId(clanMember.getMember().getId());
		
		if (foundMember != null) {
			throw new ServiceException(
					"Ya estas en un clan, debes abandonarlo"
					+ " para unirte a otro");
		}
	}

}
