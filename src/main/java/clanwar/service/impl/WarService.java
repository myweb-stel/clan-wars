package clanwar.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import clanwar.dao.IAttackDao;
import clanwar.dao.IClanDao;
import clanwar.dao.IPlayerDao;
import clanwar.dao.IWarDao;
import clanwar.dao.IWarEnemyDao;
import clanwar.dao.IWarPlayerAttackDao;
import clanwar.dao.IWarPlayerDao;
import clanwar.model.Clan;
import clanwar.model.ClanMember;
import clanwar.model.War;
import clanwar.model.WarEnemy;
import clanwar.model.WarPlayer;
import clanwar.model.WarPlayerAttack;
import clanwar.service.IWarService;

@Service
public class WarService implements IWarService {

	@Autowired
	private IWarDao warDao;
	
	@Autowired
	private IClanDao clanDao;
	
	@Autowired
	private IPlayerDao playerDao;
	
	@Autowired 
	private IWarPlayerDao warPlayerDao;
	
	@Autowired 
	private IWarPlayerAttackDao warPlayerAttackDao;
	
	@Autowired
	private IWarEnemyDao warEnemyDao;
	
	@Autowired
	private IAttackDao attackDao;
	
	@Override
	public War createNewWar(Date startDate, List<String> playersNames) throws ServiceException {
		
		List<WarPlayer> warPlayers = new ArrayList<WarPlayer>();
		List<WarEnemy> warEnemies = new ArrayList<WarEnemy>();
		War war = new War();
		
		Authentication auth = SecurityContextHolder
								.getContext().getAuthentication();
	    String creator = auth.getName(); 
		
		Clan clan = clanDao.findByPlayerUsername(playersNames.get(0));
		war.setClan(clan);
		war.setStartDate(startDate);
		
		int warNumber = 1;
		for (String username : playersNames) {
			for (ClanMember clanMember : clan.getMembers()) {
				if (username.equals(clanMember.getMember().getUsername())) {
					
					if(username.equals(creator)) {
						if(!"Leader".equals(clanMember.getRole().getValue())) {
							throw new ServiceException("No tienes permisos para crear la guerra.");
						}
					}
					
					WarPlayer warPlayer = makeNewWarPlayer(war, clanMember);
					WarEnemy warEnemy = makeNewWarEnemy(war, warNumber);
					
					warNumber++;
					
					warEnemies.add(warEnemy);
					warPlayers.add(warPlayer);
				}
			}
			
		}
		
		war.setPlayers(warPlayers);
		war.setEnemies(warEnemies);
		saveNewWar(war);
		
		return war;
	}
	
	private WarPlayer makeNewWarPlayer(War war, ClanMember clanMember) {
		WarPlayer warPlayer = new WarPlayer();
		warPlayer.setWar(war);
		warPlayer.setPlayer(clanMember.getMember());
		warPlayer.setWarNumber(clanMember.getWarNumber());
		return warPlayer;
	}
	
	private WarEnemy makeNewWarEnemy(War war, int warNumber) {
		WarEnemy warEnemy = new WarEnemy();
		warEnemy.setWar(war);
		warEnemy.setWarNumber(warNumber);
		return warEnemy;
	}
	
	private void saveNewWar(War war) {
		saveWar(war);
		for (WarEnemy warEnemy : war.getEnemies()) {
			warEnemyDao.save(warEnemy);
		}
		for (WarPlayer warPlayer : war.getPlayers()) {
			warPlayerDao.save(warPlayer);
		}
	}
	
	@Override
	public void saveWar(War war) {
		warDao.save(war);
	}
	
	@Override
	public List<War> findAllWars() {
		return warDao.findAll();
	}
	
	@Override
	public List<War> findWarsByIdClan(int idClan) {
		return warDao.findByIdClan(idClan);
	}

	@Override
	public War findCurrentWar(String username) {
		Clan clan = clanDao.findByPlayerUsername(username);
		War war = warDao.findCurrent(clan.getId());
		return war;
	}

	@Override
	public War findWarById(int id) {
		War war = warDao.findById(id);
		return war;
	}

	@Override
	public War findWarByDate(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateWarPlayer(WarPlayer warPlayer) {
		warPlayerDao.update(warPlayer);
	}
	
	@Override
	public void saveWarPlayerAttack(WarPlayerAttack warPlayerAttack) {
		// TODO borrar el guardar enemigo por que ya estara guardado
		// Y revisar esto bien
		WarEnemy enemy = warEnemyDao.findByNumberInWar(
				warPlayerAttack.getWarPlayer().getWar(), 
				warPlayerAttack.getAttack().getEnemy());
		if(enemy == null) {
			warEnemyDao.save(warPlayerAttack.getAttack().getEnemy());
		} else {
			warPlayerAttack.getAttack().setEnemy(enemy);
		}
		// ---------------------------------
		
		attackDao.save(warPlayerAttack.getAttack());
		warPlayerAttackDao.save(warPlayerAttack);
	}
	
	@Override
	public void updateWarPlayerAttack(WarPlayerAttack warPlayerAttack) {
		warPlayerAttackDao.update(warPlayerAttack);
	}
	
	@Override
	public void deleteWarPlayerAttack(WarPlayerAttack warPlayerAttack) {
		warPlayerAttackDao.delete(warPlayerAttack);
	}
	
	@Override
	public List<WarPlayerAttack> findAttacksOnEnemy(WarEnemy warEnemy) {
		return warPlayerAttackDao.findByIdEnemyAndIdWar(warEnemy.getId(), warEnemy.getWar().getId());
	}
	
}
