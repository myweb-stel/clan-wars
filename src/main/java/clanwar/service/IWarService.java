package clanwar.service;

import java.util.Date;
import java.util.List;

import javax.xml.rpc.ServiceException;

import clanwar.model.War;
import clanwar.model.WarEnemy;
import clanwar.model.WarPlayer;
import clanwar.model.WarPlayerAttack;

public interface IWarService {

	War createNewWar(Date startDate, List<String> playersNames) throws ServiceException;
	
	void saveWar(War war);
	
	List<War> findAllWars();
	
	List<War> findWarsByIdClan(int idClan);
	
	War findCurrentWar(String username);
	
	War findWarById(int id);
	
	War findWarByDate(Date date);
	
	void updateWarPlayer(WarPlayer warPlayer);
	
	void saveWarPlayerAttack(WarPlayerAttack warPlayerAttack);
	
	void updateWarPlayerAttack(WarPlayerAttack warPlayerAttack);
	
	void deleteWarPlayerAttack(WarPlayerAttack warPlayerAttack);
	
	List<WarPlayerAttack> findAttacksOnEnemy(WarEnemy warEnemy);
	
}
