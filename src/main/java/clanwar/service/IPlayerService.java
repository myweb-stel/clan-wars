package clanwar.service;

import java.util.List;

import clanwar.model.Player;
import clanwar.model.PlayerRole;
import clanwar.model.War;
import clanwar.model.WarPlayer;

public interface IPlayerService {

	void savePlayer(Player player);
	
	// TODO void updatePlayer(Player player);
	
	Player findPlayerByName(String name);
	
	void savePlayerInWar(WarPlayer warPlayer);
	
	void updatePlayerInWar(WarPlayer warPlayer);
	
	List<WarPlayer> findPlayersInWar(War war);
	
	void savePlayerRole(PlayerRole playerRole);
	
}
