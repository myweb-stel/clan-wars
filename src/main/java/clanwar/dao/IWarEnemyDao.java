package clanwar.dao;

import clanwar.common.exception.DaoException;
import clanwar.model.WarEnemy;
import clanwar.model.War;

public interface IWarEnemyDao {

	void save(WarEnemy enemy) throws DaoException;
	
	void update(WarEnemy enemy) throws DaoException;
	
	WarEnemy findByNumberInWar(War war, WarEnemy enemy) throws DaoException;
	
}
