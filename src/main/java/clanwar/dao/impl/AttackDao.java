package clanwar.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IAttackDao;
import clanwar.model.Attack;

@Repository
@Transactional
public class AttackDao extends GenericDao implements IAttackDao {

	@Override
	public void save(Attack attack) throws DaoException {
		
		try {
			getSession().save(attack);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
	}

	@Override
	public void update(Attack attack) throws DaoException {
		
		try {
			getSession().update(attack);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
	}

	@Override
	public Attack findById(int id) throws DaoException {
		
		Attack attack;
		
		try {
			attack = (Attack) getSession()
				.createQuery(Query.ATTACK_BY_ID)
				.setParameter(Query.FIELD_ID, id)
				.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return attack;
	}

}
