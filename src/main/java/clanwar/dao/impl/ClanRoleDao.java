package clanwar.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IClanRoleDao;
import clanwar.model.ClanRole;

@Repository
@Transactional
public class ClanRoleDao extends GenericDao implements IClanRoleDao {

	@Override
	public ClanRole findByValue(String value) throws DaoException {
		
		ClanRole role;
		
		try {
			role = (ClanRole) getSession()
						.createQuery(Query.CLAN_ROLE_BY_VALUE)
						.setParameter(Query.FIELD_ROLE, value)
						.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return role;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClanRole> findAll() throws DaoException {
		
		List<ClanRole> roles = new ArrayList<ClanRole>();
		
		try {
			roles = getSession()
					.createQuery(Query.CLAN_ROLE_ALL)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return roles;
	}

}
