package clanwar.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.exception.DaoException;
import clanwar.dao.IClanRequestDao;
import clanwar.model.ClanRequest;

@Repository
@Transactional
public class ClanRequestDao extends GenericDao implements IClanRequestDao {

	@Override
	public void save(ClanRequest clanRequest) throws DaoException {
		try {
			getSession().save(clanRequest);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void delete(ClanRequest clanRequest) throws DaoException {
		try {
			getSession().delete(clanRequest);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
}
