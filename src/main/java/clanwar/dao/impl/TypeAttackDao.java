package clanwar.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.ITypeAttackDao;
import clanwar.model.TypeAttack;

@Repository
@Transactional
public class TypeAttackDao extends GenericDao implements ITypeAttackDao {

	@Override
	public void save(TypeAttack typeAttack) throws DaoException {
		
		try {
			getSession().save(typeAttack);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public TypeAttack findByValue(TypeAttack typeAttack) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TypeAttack> findAll() throws DaoException {
		
		List<TypeAttack> typeAttacks;
		
		try {
			typeAttacks = getSession()
					.createQuery(Query.TYPE_ATTACK_ALL)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return typeAttacks;
	}

}
