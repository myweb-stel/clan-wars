package clanwar.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IWarDao;
import clanwar.model.War;

@Repository
@Transactional
public class WarDao extends GenericDao implements IWarDao {
	
	@Override
	public void save(War war) throws DaoException {
		
		try {
			getSession().save(war);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<War> findAll() throws DaoException {
		
		List<War> wars;
		
		try {
			wars = (List<War>) getSession()
					.createQuery(Query.WAR_ALL)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
				
		return wars;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<War> findByIdClan(int idClan) throws DaoException {
		
		List<War> wars;
		
		try {
			wars = (List<War>) getSession()
					.createQuery(Query.WARS_BY_CLAN)
					.setParameter(Query.FIELD_CLAN, idClan)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return wars;
	}

	@Override
	public War findCurrent(int idClan) throws DaoException {
		
		War war;
		
		try {
			// TODO	
			war = (War) getSession()
					.createQuery(Query.WAR_CURRENT)
					.setParameter(Query.FIELD_CLAN, idClan)
					.setMaxResults(1)
					.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return war;
	}

	@Override
	public War findById(int id) throws DaoException {
		
		War war;
		
		try {
			war = (War) getSession()
					.createQuery(Query.WAR_WHERE_ID)
					.setParameter(Query.FIELD_ID, id)
					.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return war;
	}

	@Override
	public War findByDate(Date date) throws DaoException {
		
		try {
			// TODO Auto-generated method stub
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return null;
	}

}