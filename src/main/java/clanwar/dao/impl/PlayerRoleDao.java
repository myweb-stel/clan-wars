package clanwar.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IPlayerRoleDao;
import clanwar.model.PlayerRole;

@Repository
@Transactional
public class PlayerRoleDao extends GenericDao implements IPlayerRoleDao {

	@Override
	public void save(PlayerRole role) throws DaoException {
		try {
			getSession().save(role);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void update(PlayerRole role) throws DaoException {
		try {
			getSession().save(role);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PlayerRole> findAll() throws DaoException {
		
		List<PlayerRole> roles = new ArrayList<PlayerRole>();
		
		try {
			roles = getSession()
					.createQuery(Query.PLAYER_ROLE_ALL)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return roles;
	}

}
