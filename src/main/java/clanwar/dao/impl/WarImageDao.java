package clanwar.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.exception.DaoException;
import clanwar.dao.IWarImageDao;
import clanwar.model.WarImage;

@Repository
@Transactional
public class WarImageDao extends GenericDao implements IWarImageDao {

	@Override
	public void save(WarImage warImage) throws DaoException {
		try {
			getSession().save(warImage);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void update(WarImage warImage) throws DaoException {
		try {
			getSession().save(warImage);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

}
