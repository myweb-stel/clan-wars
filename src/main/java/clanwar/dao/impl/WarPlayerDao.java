package clanwar.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IWarPlayerDao;
import clanwar.model.WarPlayer;
import clanwar.model.War;

@Repository
@Transactional
public class WarPlayerDao extends GenericDao implements IWarPlayerDao {

	@Override
	public void save(WarPlayer warPlayer) throws DaoException {
		
		try {
			getSession().save(warPlayer);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
	}
	
	@Override
	public void update(WarPlayer warPlayer) throws DaoException {
		
		try {
			getSession().update(warPlayer);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
			
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WarPlayer> findPlayersInWar(War war) throws DaoException {
		
		List<WarPlayer> warPlayers;
		
		try {
			warPlayers = getSession()
					.createQuery(Query.PLAYERS_IN_WAR)
					.setParameter(Query.FIELD_WAR, war.getId()).
					list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return warPlayers;
	}
	
}
