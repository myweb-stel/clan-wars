package clanwar.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IClanMemberDao;
import clanwar.model.ClanMember;

@Repository
@Transactional
public class ClanMemberDao extends GenericDao implements IClanMemberDao {

	@Override
	public void save(ClanMember member) throws DaoException {
		try {
			getSession().save(member);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void update(ClanMember member) throws DaoException {
		try {
			getSession().update(member);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@Override
	public void delete(ClanMember member) throws DaoException {
		try {
			getSession().delete(member);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@Override
	public ClanMember find(ClanMember member) throws DaoException {
		
		ClanMember foundMember;	
		// TODO 
		try {
			foundMember = (ClanMember) getSession()
				.createSQLQuery("select * from CLAN_MEMBERS where CLAN = :clan and MEMBER = :member")
				.setParameter("clan", member.getClan().getId())
				.setParameter("member", member.getMember().getId())
				.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return foundMember;
	}
	
	@Override
	public ClanMember findByPlayerId(int id) throws DaoException {
		
		ClanMember foundMember;
		
		// TODO querys con CLAN o CLANMEMBER se hace recursivas. (Se ejecuta la misma query 100 veces)
		// ¿Por que? no lo se.
		
		try {
			foundMember = (ClanMember) getSession()
				.createQuery(Query.CLAN_MEMBER_WHERE_MEMBER)
				.setParameter(Query.FIELD_MEMBER, id)
				.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return foundMember;
	}
	
}
