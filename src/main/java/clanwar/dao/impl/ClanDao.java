package clanwar.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IClanDao;
import clanwar.model.Clan;

@Repository
@Transactional
public class ClanDao extends GenericDao implements IClanDao {

	@Override
	public void save(Clan clan) throws DaoException {
		try {
			getSession().save(clan);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@Override
	public void update(Clan clan) throws DaoException {
		try {
			getSession().update(clan);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Clan> findAll() throws DaoException {
		
		List<Clan> clans;
		
		try {
			clans = getSession()
						.createQuery(Query.CLAN_ALL)
						.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return clans;
	}
	
	@Override
	public Clan findById(int id) throws DaoException {
		
		Clan foundClan;
		
		try {
			foundClan = (Clan) getSession()
					.createQuery(Query.CLAN_BY_ID)
					.setParameter(Query.FIELD_ID, id)
					.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return foundClan;
	}
	
	@Override
	public List<Clan> findByName(Clan clan) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Clan findByPlayerId(int id) throws DaoException {
		
		Clan foundClan;
		
		try {
			foundClan = (Clan) getSession()
					.createQuery(Query.CLAN_BY_PLAYER_ID)
					.setParameter(Query.FIELD_MEMBER, id)
					.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return foundClan;		
	}
	
	@Override
	public Clan findByPlayerUsername(String username) throws DaoException {
		
		Clan foundClan;
		
		try {
			foundClan = (Clan) getSession()
					.createQuery(Query.CLAN_BY_PLAYER_USERNAME)
					.setParameter(Query.FIELD_USERNAME, username)
					.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return foundClan;	
	}
	
}