package clanwar.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.ITimeRangeDao;
import clanwar.model.TimeRange;

@Repository
@Transactional
public class TimeRangeDao extends GenericDao implements ITimeRangeDao {

	@Override
	public TimeRange findByValue(TimeRange timeRange) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeRange> findAll() throws DaoException {
		
		List<TimeRange> timesRanges;
		
		try {
			timesRanges = getSession()
					.createQuery(Query.TIMES_RANGE_ALL)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return timesRanges;
		
	}

}
