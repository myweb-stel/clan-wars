package clanwar.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.exception.DaoException;
import clanwar.dao.IWarPlayerAttackDao;
import clanwar.model.WarPlayerAttack;

@Repository
@Transactional
public class WarPlayerAttackDao extends GenericDao implements IWarPlayerAttackDao {

	@Override
	public void save(WarPlayerAttack warPlayerAttack) throws DaoException {
		try {
			getSession().save(warPlayerAttack);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void update(WarPlayerAttack warPlayerAttack) throws DaoException {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void delete(WarPlayerAttack warPlayerAttack) throws DaoException {
		// TODO Auto-generated method stub
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WarPlayerAttack> findByIdEnemyAndIdWar(int idEnemy, int idWar) throws DaoException {
		// TODO REMOVE O SOLVE THAT!!
		List<WarPlayerAttack> attacks = null;
		
		try {
			
			attacks = getSession()
				.createQuery("from WarPlayerAttack wpa where wpa.attack.enemy.id in :idEnemy and wpa.attack.enemy.war.id in :idWar")
				.setParameter("idEnemy", idEnemy)
				.setParameter("idWar", idWar)
				//.createSQLQuery("select * from WAR_PLAYERS_ATTACKS where ATTACK in (select ID from ATTACKS where ENEMY in (select ID from WAR_ENEMIES where ID = ? and WAR = ?))")
				//.setParameter(0, idEnemy)
				//.setParameter(1, idWar)
				.list();
			
			System.out.println(attacks.size());
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return attacks;
	}

}
