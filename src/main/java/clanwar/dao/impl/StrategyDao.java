package clanwar.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IStrategyDao;
import clanwar.model.Strategy;

@Repository
@Transactional
public class StrategyDao extends GenericDao implements IStrategyDao {

	@Override
	public Strategy findByValue(Strategy strategy) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Strategy> findAll() throws DaoException {
		
		List<Strategy> strategies;
		
		try {
			strategies = getSession()
					.createQuery(Query.STRATEGY_ALL)
					.list();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return strategies;
	}

}
