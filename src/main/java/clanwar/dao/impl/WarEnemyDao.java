package clanwar.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IWarEnemyDao;
import clanwar.model.WarEnemy;
import clanwar.model.War;

@Repository
@Transactional
public class WarEnemyDao extends GenericDao implements IWarEnemyDao {

	@Override
	public void save(WarEnemy enemy) throws DaoException {
		try {
			getSession().save(enemy);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public void update(WarEnemy enemy) throws DaoException {
		try {
			getSession().update(enemy);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	@Override
	public WarEnemy findByNumberInWar(War war, WarEnemy enemy) throws DaoException {
		
		WarEnemy foundEnemy = null;
		
		try {
			foundEnemy = (WarEnemy) getSession()
				.createQuery(Query.ENEMY_NUMBER_IN_WAR)
				.setParameter(Query.FIELD_WAR, war.getId())
				.setParameter(Query.FIELD_WAR_NUMBER, enemy.getWarNumber())
				.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return foundEnemy;
	}

}
