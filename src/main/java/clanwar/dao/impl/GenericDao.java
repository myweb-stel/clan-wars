package clanwar.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class GenericDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	protected Session getSession() {
		Session session = sessionFactory.getCurrentSession();
		return session;
	}

}
