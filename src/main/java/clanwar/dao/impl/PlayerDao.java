package clanwar.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import clanwar.common.Query;
import clanwar.common.exception.DaoException;
import clanwar.dao.IPlayerDao;
import clanwar.model.Player;

@Repository
@Transactional
public class PlayerDao extends GenericDao implements IPlayerDao {

	@Override
	public void save(Player player) throws DaoException {
		
		try {
			getSession().save(player);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@Override
	public void update(Player player) throws DaoException {
		try {
			getSession().update(player);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	@Override
	public Player findByName(String name) throws DaoException {
		
		Player player;
		
		try {
			player = (Player) getSession()
				.createQuery(Query.PLAYER_WHERE_USERNAME)
				.setParameter(Query.FIELD_USERNAME, name)
				.uniqueResult();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		
		return player;
	}

}
