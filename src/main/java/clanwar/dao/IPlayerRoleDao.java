package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.PlayerRole;

public interface IPlayerRoleDao {
	
	void save(PlayerRole role) throws DaoException;
	
	void update(PlayerRole role) throws DaoException;
	
	List<PlayerRole> findAll() throws DaoException;
	
}
