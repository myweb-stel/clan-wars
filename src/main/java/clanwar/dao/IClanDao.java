package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.Clan;

public interface IClanDao {

	void save(Clan clan) throws DaoException;
	
	void update(Clan clan) throws DaoException;
	
	List<Clan> findAll() throws DaoException;
	
	Clan findById(int id) throws DaoException;
	
	List<Clan> findByName(Clan clan) throws DaoException;
	
	Clan findByPlayerId(int id) throws DaoException;
	
	Clan findByPlayerUsername(String username) throws DaoException;
	
}
