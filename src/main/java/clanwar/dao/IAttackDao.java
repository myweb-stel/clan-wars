package clanwar.dao;

import clanwar.common.exception.DaoException;
import clanwar.model.Attack;

public interface IAttackDao {

	void save(Attack attack) throws DaoException;
	
	void update(Attack attack) throws DaoException;
	
	Attack findById(int id) throws DaoException;
	
}
