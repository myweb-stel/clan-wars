package clanwar.dao;

import clanwar.common.exception.DaoException;
import clanwar.model.ClanRequest;

public interface IClanRequestDao {

	void save(ClanRequest clanRequest) throws DaoException;
	
	void delete(ClanRequest clanRequest) throws DaoException;
	
}
