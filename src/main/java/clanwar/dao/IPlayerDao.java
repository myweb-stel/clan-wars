package clanwar.dao;

import clanwar.common.exception.DaoException;
import clanwar.model.Player;

public interface IPlayerDao {

	void save(Player player) throws DaoException;
	
	void update(Player player) throws DaoException;
	
	Player findByName(String name) throws DaoException;
	
}
