package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.Strategy;

public interface IStrategyDao {
	
	Strategy findByValue(Strategy strategy) throws DaoException;

	List<Strategy> findAll() throws DaoException;
	
}
