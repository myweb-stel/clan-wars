package clanwar.dao;

import clanwar.common.exception.DaoException;
import clanwar.model.WarImage;

public interface IWarImageDao {
	
	void save(WarImage warImage) throws DaoException;
	
	void update(WarImage warImage) throws DaoException;

}
