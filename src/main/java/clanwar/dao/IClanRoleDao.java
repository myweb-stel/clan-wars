package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.ClanRole;

public interface IClanRoleDao {

	ClanRole findByValue(String value) throws DaoException;
	
	List<ClanRole> findAll() throws DaoException;
	
}
