package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.WarPlayerAttack;

public interface IWarPlayerAttackDao {

	void save(WarPlayerAttack warPlayerAttack) throws DaoException;
	
	void update(WarPlayerAttack warPlayerAttack) throws DaoException;
	
	void delete(WarPlayerAttack warPlayerAttack) throws DaoException;
	
	List<WarPlayerAttack> findByIdEnemyAndIdWar(int idEnemy, int idWar) throws DaoException;
	
}
