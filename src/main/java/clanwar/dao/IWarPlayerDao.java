package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.War;
import clanwar.model.WarPlayer;

public interface IWarPlayerDao {
	
	void save(WarPlayer warPlayer) throws DaoException;
	
	void update(WarPlayer warPlayer) throws DaoException;
	
	List<WarPlayer> findPlayersInWar(War war) throws DaoException;

}
