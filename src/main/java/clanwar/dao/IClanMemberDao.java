package clanwar.dao;

import clanwar.common.exception.DaoException;
import clanwar.model.ClanMember;

public interface IClanMemberDao {

	void save(ClanMember member) throws DaoException;
	
	void update(ClanMember member) throws DaoException;
	
	void delete(ClanMember member) throws DaoException;
	
	ClanMember find(ClanMember member) throws DaoException;
	
	ClanMember findByPlayerId(int id) throws DaoException;
	
}
