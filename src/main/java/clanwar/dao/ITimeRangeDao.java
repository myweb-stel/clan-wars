package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.TimeRange;

public interface ITimeRangeDao {

	TimeRange findByValue(TimeRange timeRange) throws DaoException;
	
	List<TimeRange> findAll() throws DaoException;
	
}
