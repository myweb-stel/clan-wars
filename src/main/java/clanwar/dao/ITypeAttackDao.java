package clanwar.dao;

import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.TypeAttack;

public interface ITypeAttackDao {

	void save(TypeAttack typeAttack) throws DaoException;
	
	TypeAttack findByValue(TypeAttack typeAttack) throws DaoException;
	
	List<TypeAttack> findAll() throws DaoException;
	
}
