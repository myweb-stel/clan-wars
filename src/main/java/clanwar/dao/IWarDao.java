package clanwar.dao;

import java.util.Date;
import java.util.List;

import clanwar.common.exception.DaoException;
import clanwar.model.War;

public interface IWarDao {

	void save(War war) throws DaoException;
	
	List<War> findAll() throws DaoException;
	
	List<War> findByIdClan(int idClan) throws DaoException;
	
	War findCurrent(int idClan) throws DaoException;
	
	War findById(int id) throws DaoException;
	
	War findByDate(Date date) throws DaoException;
	
}
