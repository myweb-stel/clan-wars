package clanwar.common;

public class Query {

	/* Fields */
	public final static String FIELD_ID = "id";
	public final static String FIELD_USERNAME = "username";
	public final static String FIELD_WAR = "war";
	public final static String FIELD_WAR_NUMBER = "war_number";
	public final static String FIELD_ROLE = "role";
	public final static String FIELD_MEMBER = "member";
	public final static String FIELD_CLAN = "clan";
	
	
	/* Query Attacks */
	public final static String ATTACK_BY_ID = "from Attack a where a.id = :id";
	
	
	/* Query Clans */
	public final static String CLAN_ALL = "from Clan";
	public final static String CLAN_BY_ID = "from Clan where id = :" + FIELD_ID;
	// CLAN_BY_NAME
	public final static String CLAN_BY_PLAYER_ID = "from Clan c where c.id "
			+ "= (select cm.clan from ClanMember cm where cm.member.id = :member)";
	public final static String CLAN_BY_PLAYER_USERNAME = "from Clan c where c.id "
			+ "= (select cm.clan from ClanMember cm where cm.member.id "
			+ "= (select p.id from Player p where p.username = :username))";
	
	// CLAN_MEMBER_BY_CLAN_AND_PLAYER
	public final static String CLAN_MEMBER_WHERE_MEMBER = "from ClanMember cm where cm.member.id = :" + FIELD_MEMBER;
	
	public final static String CLAN_ROLE_BY_VALUE = "from ClanRole where ROLE = :" + FIELD_ROLE;
	public final static String CLAN_ROLE_ALL = "from ClanRole";
	
	
	/* Query Players */
	public final static String PLAYER_WHERE_USERNAME = "from Player p where USERNAME = :" + FIELD_USERNAME;
	
	public final static String PLAYER_ROLE_ALL = "from PlayerRole";
	
	
	/* Query Strategies */
	public final static String STRATEGY_ALL = "from Strategy";
	
	
	/* Query TimeRanges */
	public final static String TIMES_RANGE_ALL = "from TimeRange";
	
	
	/* Query TypeAttacks */
	public final static String TYPE_ATTACK_ALL = "from TypeAttack";
	public final static String PLAYERS_IN_WAR = "from WarPlayer where WAR = :" + FIELD_WAR + " order by WAR_NUMBER";
	
	
	/* Query War */
	public final static String WAR_ALL = "from War";
	public final static String WARS_BY_CLAN = "from War w where w.clan.id = :clan";
	public final static String WAR_CURRENT = "from War w where w.clan.id = :clan ORDER BY w.startDate desc"; 
	public final static String WAR_WHERE_ID = "from War where id = :" + FIELD_ID;

	public final static String ENEMY_NUMBER_IN_WAR = "from WarEnemy where WAR = :" + FIELD_WAR + " and WAR_NUMBER = :" + FIELD_WAR_NUMBER;
		
}
