package clanwar.common;

public class Name {
	
	public class View {
		
		public final static String HOME = "home";
		public final static String NOT_FOUND = "not-found";
		public final static String DENIED = "denied";
		public final static String LOGIN = "login";
		public final static String SIGNUP = "signup";
		public final static String WAR = "war";
		public final static String WAR_SHOW = "war-show";
		public final static String PROFILE = "profile";
		public final static String CLAN = "clan";
		
	}
	
	public class Key {
		
		public final static String ERROR = "error";
		public final static String INFO = "info";
		public final static String USERNAME = "username";
		public final static String URL = "url";
		
	}
	
	public class URL {
		
		public final static String WAR_CURRENT = "/json/war/current";
		
	}
	
}
