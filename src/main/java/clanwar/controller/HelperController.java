package clanwar.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import clanwar.model.Clan;
import clanwar.model.ClanMember;
import clanwar.model.ClanRole;
import clanwar.model.Player;
import clanwar.model.PlayerRole;
import clanwar.service.IClanService;
import clanwar.service.ICommonService;
import clanwar.service.IPlayerService;

/**
 * Controller para hacer cosas que luego se van a ser temporales
 * en PRO no deberia existir
 *
 */
@Controller
public class HelperController {
	
	@Autowired
	private IPlayerService playerService;
	
	@Autowired
	private IClanService clanService;
	
	@Autowired
	private ICommonService commonService;
	
	@RequestMapping(value = "/createPinpinelas")
	public void playerSaveHandler() {
		
		Clan clan = new Clan();
		clan.setName("Pinpinelas");
		clanService.saveClan(clan, "Artur0");
		
		ArrayList<String> players = new ArrayList<String>();
		players.add("Askiji");
		players.add("JavitoBravs");
		
		players.add("Rayen1992");
		players.add("sans");
		players.add("herbods");		
		players.add("Wyzer");
		players.add("Manute");
		players.add("Albero");
		players.add("sumakah");
		players.add("kasiu");		
		players.add("chilin");
		players.add("Baze");
		players.add("Rivers");
		players.add("Migueeel19");
		
		for (String name : players) {
			
			Player player = new Player();
			player.setUsername(name);
			player.setPassword("123456");
			player.setEnabled(true);
			player.setAccountNonExpired(true);
			player.setAccountNonLocked(true);
			player.setCredentialsNonExpired(true);
			
			PlayerRole role = new PlayerRole();
			role.setPlayer(player);
			role.setRole("ROLE_USER");
			
			ClanMember clanMember = new ClanMember();
			clanMember.setClan(clan);
			clanMember.setMember(player);
			
			ClanRole clanRole = new ClanRole();
			clanRole.setId(1);
			
			if (name.equals("Askiji")) {
				clanRole.setId(3);
			}
			
			clanMember.setRole(clanRole);
			
			playerService.savePlayer(player);
			playerService.savePlayerRole(role);
			clanService.joinClan(clanMember);
		}
		
	}
	
}
