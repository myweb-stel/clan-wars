package clanwar.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import clanwar.common.Name;
import clanwar.model.WarPlayer;
import clanwar.model.WarPlayerAttack;
import clanwar.service.ICommonService;
import clanwar.service.IWarService;

@Controller
@RequestMapping(value = "/war")
public class WarController {

	@Autowired
	private IWarService warService;
	
	@Autowired
	private ICommonService commonService;
	
	@RequestMapping(value = { "", "/" })
	public String warHandler(
			ModelMap map,
			Principal user) {
		
		map.put(Name.Key.USERNAME, user.getName());
		
		return Name.View.WAR;
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public String newWarHandler(
			ModelMap map,
			Principal user,
			@RequestParam(value = "name", required = false) List<String> playersNames) {
		
		String view = null;
		
		try {
			
			if(playersNames != null && playersNames.size() > 0) {
				warService.createNewWar(new Date(), playersNames);
				view = "redirect:/war/current";
			} else {
				view = "redirect:/war/";
				map.put(Name.Key.ERROR, "Debes seleccionar al menos un jugador para crear la guerra.");
			}
			
		} catch (Exception e) {
			
 			view = Name.View.WAR;
			map.put(Name.Key.ERROR, e.getMessage());
			
		}
		
		return view;
	}
	
	@RequestMapping(value = "/search")
	public String searchWarHandler() {
		// TODO
		return Name.View.WAR_SHOW;
	}
	
	@RequestMapping(value = "/current")
	public String currentWarHandler(
			ModelMap map,
			Principal user) {
		
		map.put(Name.Key.URL, Name.URL.WAR_CURRENT);
		map.put(Name.Key.USERNAME, user.getName());
		
		return Name.View.WAR_SHOW;
	}
	
	@RequestMapping(value = "/saveplayer", method = RequestMethod.POST)
	@ResponseBody
	public String saveWarPlayerAttackHandler(
			@ModelAttribute("warPlayer") WarPlayer warPlayer) {
		// TODO
		String updated = "ok";
		
		try {
			warService.updateWarPlayer(warPlayer);
		} catch (Exception e) {
			updated = "error";
		}
		
		return updated;
	}
	
	@RequestMapping(value = "/saveattack", method = RequestMethod.POST)
	public String saveWarPlayerAttackHandler(
			@ModelAttribute("warPlayerAttack") WarPlayerAttack warPlayerAttack) {
		// TODO
		warService.saveWarPlayerAttack(warPlayerAttack);
		return "redirect:/war/current";
	}
	
}
