package clanwar.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import clanwar.common.Name;
import clanwar.model.Player;
import clanwar.service.IClanService;
import clanwar.service.IPlayerService;

@Controller
@RequestMapping(value = "/player")
public class PlayerController {

	@Autowired
	private IPlayerService playerService;
	
	@Autowired
	private IClanService clanService;
	
	@Autowired
	private JsonController json;
	
	@RequestMapping(value = "/profile")
	public String profileHandler(
			ModelMap map,
			Principal user) {
		
		map.put("username", user.getName());
		
		return Name.View.PROFILE;
	}
	
	@RequestMapping(value = "/password")
	public String passwordHandler() {
		// TODO
		return null;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String playerUdapteHandler(
			@ModelAttribute("player") Player player) {
		// TODO
		return null;
	}
	
}
