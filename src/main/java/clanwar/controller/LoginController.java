package clanwar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import clanwar.common.Name;
import clanwar.model.Player;
import clanwar.service.IPlayerService;

@Controller
public class LoginController {

	@Autowired
	private IPlayerService playerService;
	
	@RequestMapping(value = "/login")
	public String playerLoginHandler(
			ModelMap map,
			@RequestParam(value = "error", required = false) String error) {
		
		if (error != null) {
			map.put(Name.Key.ERROR, "Usuario o contrase\u00F1a incorrectos");
		}
		
		return Name.View.LOGIN;
	}
	
	@RequestMapping(value = "/logout")
	public String logoutHandler() {
		return "redirect:/login";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUpViewHandler() {
		// TODO
		return Name.View.SIGNUP;
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signUpHandler(
			ModelMap map,
			@ModelAttribute("player") Player player) {
		
		String view = null;
		
		try {
			
			playerService.savePlayer(player);
			view = "redirect:/login"; 
			
		} catch (Exception e) {
			
			view = Name.View.SIGNUP;
			map.put(Name.Key.ERROR, e.getMessage());
			
		}
		
		return view;
	}
	
	
}
