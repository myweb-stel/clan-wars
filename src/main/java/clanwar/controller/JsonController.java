package clanwar.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import clanwar.model.Clan;
import clanwar.model.War;
import clanwar.model.WarEnemy;
import clanwar.model.WarPlayerAttack;
import clanwar.service.IClanService;
import clanwar.service.ICommonService;
import clanwar.service.IWarService;

@Controller
@RequestMapping(value = "/json", produces = MediaType.APPLICATION_JSON_VALUE)
public class JsonController {
	
	@Autowired
	private IWarService warService;
	
	@Autowired
	private IClanService clanService;
	
	@Autowired
	private ICommonService commonService;
	
	@RequestMapping(value = "/war")
	@ResponseBody
	public War jsonWarHandler(
			@RequestParam(value = "id") int id) {
		
		return warService.findWarById(id);
	}
	
	@RequestMapping(value = "/war/current")
	@ResponseBody
	public War jsonWarHandler(Principal user) {
		return warService.findCurrentWar(user.getName());
	}
	
	@RequestMapping(value = "/wars")
	@ResponseBody
	public List<War> jsonWarsHandler(
			Principal user,
			@RequestParam(value = "idClan", required = false) String idClan) {
		
		List<War> wars;
		
		if (idClan != null) {
			Clan clan = clanService.findClanByPlayerUsername(user.getName());
			wars = warService.findWarsByIdClan(clan.getId());
		} else {
			wars = warService.findAllWars();
		}
		
		// TODO no permite serializar el objeto 'Clan'
		// si no se hace esto. Corregir.
		for (War war : wars) {
			war.setClan(null);
		}
		
		return wars;
	}
	
	@RequestMapping(value = "/war/attacks")
	@ResponseBody
	public List<WarPlayerAttack> jsonWarAttacksHandler(
			@RequestParam(value = "idWar") String idWar,
			@RequestParam(value = "idEnemy") String idEnemy) {
		
		// TODO cambiar metodo en war service para que no tenga que recibir un war enemy.
		War war = new War();
		war.setId(Integer.valueOf(idWar));
		WarEnemy warEnemy = new WarEnemy();
		warEnemy.setId(Integer.valueOf(idEnemy));
		warEnemy.setWar(war);
		
		return warService.findAttacksOnEnemy(warEnemy);
	}	
	
	@RequestMapping(value = "/clan")
	@ResponseBody
	public Clan jsonClanHandler(
			@RequestParam(value = "idClan", required = false) String idClan,
			@RequestParam(value = "player", required = false) String player) {
		
		Clan clan = null;
		
		if (player != null) {
			clan = clanService.findClanByPlayerUsername(player);
		} else if (idClan != null) {
			clan = clanService.findClandByPlayerId(Integer.valueOf(idClan));
		}
		
		return clan;
	}
	
	@RequestMapping(value = "/clans")
	@ResponseBody
	public List<Clan> jsonClansHandler() {
		
		return clanService.findAllClans();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/enums")
	@ResponseBody
	public Map jsonEnumsHandler() {
		
		Map enums = new HashMap<String, List<Object>>();
		enums.put("strategies", commonService.findAllStrategies());
		enums.put("timesRanges", commonService.findAllTimesRanges());
		enums.put("typesAttacks", commonService.findAllTipesAttacks());
		
		return enums;
	}

}
