package clanwar.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import clanwar.common.Name;
import clanwar.model.Clan;
import clanwar.model.ClanMember;
import clanwar.model.ClanRequest;
import clanwar.model.Player;
import clanwar.service.IClanService;
import clanwar.service.IPlayerService;

@Controller
@RequestMapping(value = "/clan")
public class ClanController {

	@Autowired
	private IClanService clanService;
	
	@Autowired
	private IPlayerService playerService;
	
	@RequestMapping(value = { "", "/" })
	public String clanHandler() {
		return Name.View.CLAN;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String clanNewHandler(
			Principal user,
			@ModelAttribute("clan") Clan clan) {
		
		clanService.saveClan(clan, user.getName());
		
		return "redirect:/player/profile";
	}
	
	@RequestMapping(value = "/view")
	public ModelAndView clanViewHandler() {
		// TODO
		return null;
	}
	
	@RequestMapping(value = "request", method = RequestMethod.POST)
	public String clanRequestHandler(
			Principal user,
			@ModelAttribute ClanRequest clanRequest) {
		
		try {
			clanService.saveClanRequest(clanRequest, user.getName());
		} catch (Exception e) {
			
		}
		
		return "redirect:/player/profile";
	}
	
	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public String clanJoinHandler(
			ModelMap map,
			Principal user,
			@RequestParam("idClan") int idClan,
			@RequestParam("idPlayer") int idPlayer,
			@RequestParam("allow") boolean allow) {
		
		String view = null;
		Player player = new Player();
		Clan clan = new Clan();
		
		player.setId(idPlayer);
		clan.setId(idClan);
		
		try {
			// TODO revisar por que no llega el ModelAttribute y la peticion viene exa desde core-ajax
			// TODO borrar todos los posibles request que ha podido hacer el jugador
			//member.setMember(playerService.findPlayerByName(user.getName()));
			if (allow) {
				
				ClanMember member = new ClanMember();
				member.setClan(clan);
				member.setMember(player);
				
				clanService.joinClan(member);
				view = "redirect:/player/profile";
			}
			
			ClanRequest clanRequest = new ClanRequest();
			clanRequest.setClan(clan);
			clanRequest.setPlayer(player);
			clanService.deleteClanRequest(clanRequest);
		
		} catch (Exception e) {
			view = Name.View.CLAN;
			map.put(Name.Key.ERROR, e.getMessage());
		}
		
		return view;
	}
	
	@RequestMapping(value = "/leave")
	public String clanLeaveHandler(
			Principal user) {
		
		Player player = playerService.findPlayerByName(user.getName());
		ClanMember member = clanService.findMemberByPlayerId(player.getId());
		clanService.leaveClan(member);
		
		return "redirect:/player/profile";
	}
	
	@RequestMapping(value = "/member/update", method = RequestMethod.POST)
	@ResponseBody
	public String clanMemberUpdate(
			@ModelAttribute("clanMember") ClanMember clanMember) {
		// TODO
		String updated = "ok";
		
		try {
			clanService.updateWarNumber(clanMember);
		} catch (Exception e) {
			updated = "error";
		}
		
		return updated;
	}
	
	@RequestMapping(value = "/member/allowedit", method = RequestMethod.POST)
	@ResponseBody
	public boolean clanMemberAllowEdit(
			Principal user) {
		// TODO pasar clan y comprobar en el servicio que el usuario esta en el clan
		boolean allow = false;
		
		try {
			allow = clanService.allowEdit(user.getName());
		} catch (Exception e) {
			allow = false;
		}
		
		return allow;
	}
	
}
