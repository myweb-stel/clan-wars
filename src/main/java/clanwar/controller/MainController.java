package clanwar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import clanwar.common.Name;

@Controller
public class MainController {
	
	@RequestMapping(value = "/")
	public String homeHandler() {
		return Name.View.HOME;		
	}
	
	@RequestMapping(value = "/denied")
	public String deniedHandler() {
		return Name.View.DENIED;
	}
	
	@RequestMapping(value = "/notfound")
	public String notFoundHandler() {
		return Name.View.NOT_FOUND;
	}
}
