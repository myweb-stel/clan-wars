<link rel="import" href='${pageContext.request.contextPath}/web/components/polymer/polymer.html'>
<link rel="import" href="${pageContext.request.contextPath}/web/components/font-roboto/roboto.html">

<link rel="import" href='${pageContext.request.contextPath}/web/components/core-drawer-panel/core-drawer-panel.html'>
<link rel="import" href='${pageContext.request.contextPath}/web/components/core-toolbar/core-toolbar.html'>
<link rel="import" href='${pageContext.request.contextPath}/web/components/core-menu/core-menu.html'>
<link rel="import" href='${pageContext.request.contextPath}/web/components/core-item/core-item.html'>

<link rel="import" href='${pageContext.request.contextPath}/web/components/paper-icon-button/paper-icon-button.html'>

<link rel="import" href="${pageContext.request.contextPath}/web/components/core-elements/core-elements.html">

<polymer-element
	name="layout-main"
	attributes="logged selectedMenu token path">

	<template>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/web/css/layout-main.css">

		<core-drawer-panel transition id="core_drawer_panel" touch-action>

			<core-header-panel id="nav_menu" mode="seamed" navigation drawer>

				<core-toolbar id="nav_header">
					<span>CLAN WAR</span>
				</core-toolbar>

				<core-menu
					selected="{{ selectedMenu }}">
					
					<template repeat="{{ option in menu }}">
						<core-item icon="{{ option.icon }}" label="{{ option.label }}">
							<a href="{{ option.url }}" target="_self"></a>
						</core-item>
					</template>
					
					<template if="{{ logged }}">
						<core-item
							label="Logout"
							icon="account-box"
							on-tap="{{ logout }}">
						</core-item>
					</template>

				</core-menu>

			</core-header-panel>

			<core-header-panel mode="seamed" main>

				<core-toolbar id="header">

					<paper-icon-button
						id="menu_icon" 
						icon="menu" 
						on-tap="{{ togglePanel }}">
					</paper-icon-button>
					
					<div flex>
						<template repeat="{{ optionHeader in menu }}">
							<template if="{{ optionHeader.pos == selectedMenu }}">
							<core-item icon="{{ optionHeader.icon }}"></core-icon>
								{{ optionHeader.label }}
							</core-item>
							</template>
						</template>
					</div>

					<paper-icon-button 
						id="search_icon" 
						icon="search" 
						on-tap="{{ toogleSearch }}">
					</paper-icon-button>

				</core-toolbar>

				<div horizontal center-justified layout>
					<div class="content">
						
						<content></content>
							
					</div>
				</div>

			</core-header-panel>

		</core-drawer-panel>
		
		<form id="navigate" method="GET"></form>
		
		<form id="formLogout" action="{{ path }}/logout" method="POST">
			<input type="text" name="_csrf" value="{{ token }}" />
		</form>

	</template>

	<script>
		Polymer('layout-main', {

			ready : function() {
				if (this.logged == null) {
					this.menu = [
						{pos: 0, label: "Profile", icon: "settings", url: this.path + "/player/profile"},
						{pos: 1, label: "War", icon: "event", url: this.path + "/war/"},
						{pos: 2, label: "Clan", icon: "home", url: this.path + "/clan/"},
						{pos: 3, label: "Login", icon: "account-box", url: this.path + "/login"}
					];
				} else {
					this.menu = [
						{pos: 0, label: "Profile", icon: "settings", url: this.path + "/player/profile"},
						{pos: 1, label: "War", icon: "event", url: this.path + "/war/"},
						{pos: 2, label: "Clan", icon: "home", url: this.path + "/clan/"}
					];
				}
				
			},

			togglePanel : function() {
				this.$.core_drawer_panel.togglePanel();
			},
			
			toPage: function(event, detail, sender) {
				console.log(sender.templateInstance.model.option.url);
				this.$.navigate.submit();
			},
			
			logout: function() {
				this.$.formLogout.submit();
			}

		});
	</script>

</polymer-element>
