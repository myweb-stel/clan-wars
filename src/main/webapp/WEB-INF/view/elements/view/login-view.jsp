<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-input/paper-input-decorator.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-button/paper-button.html">

<polymer-element name="login-view" on-keypress="{{ loginEnter }}">

	<template>
		
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<template if="{{ error }}">
			<div id="error" horizontal center-justified layout class="error">
				{{ error }}
			</div>
		</template>
		
		<div horizontal center-justified layout>
			
			<c:url var="urlLogin" value="/login"></c:url>
			<form id="formLogin" method="POST" action="${urlLogin}">
			
				<paper-input-decorator 
					floatingLabel
					label="Username:">
					<input
						id="inputUsername"
						is="core-input"
						type="text" 
						name="username"
						value="{{ username }}"> 
				</paper-input-decorator> <br />
					
				<paper-input-decorator 
					floatingLabel
					label="Password:">
					<input
						is="core-input"
						type="password" 
						name="password"
						value="{{ password }}">
				</paper-input-decorator> <br />
				
				<input 
					type="hidden" 
					name="${_csrf.parameterName}" 
					value="${_csrf.token}" />
				
				<div horizontal center-justified layout>
					<paper-button
						class="button-raised"
						raised
						on-tap="{{ login }}">
						Entrar
					</paper-button>
				</div>
			
			</form>
		</div>
		
	</template>

	<script>

		Polymer('login-view', {
			
			ready: function() {
				this.error = '${error}';
				this.$.inputUsername.focus();
			},
			
			login: function() {
				this.$.formLogin.submit();
			},
			
			loginEnter: function(event, detail, sender) {
				if(event.keyCode == 13) {
					this.login();
				}
			}
						
		});
		
	</script>

</polymer-element>