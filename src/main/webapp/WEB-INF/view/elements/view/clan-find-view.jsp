<%@ include file="../model/clan-model.jsp"%>

<polymer-element 
	name="clan-find-view"
	attributes="csrf">

	<template>
		
		<div center-justified layout>
			<template repeat="{{ clan in response }}">
				
				<div>
					<clan-model 
						clan="{{ clan }}"
						csrf="{{ csrf }}"></clan-model>
				</div>
				
			</template>
		</div>
		
		<core-ajax
			id="getClans"
			url="${pageContext.request.contextPath}/json/clans"
			handleAs="json"
			response="{{ response }}">
		</core-ajax>
		
	</template>

	<script>

		Polymer('clan-find-view', {
			
			ready: function() {
				this.$.getClans.go();
			}
			
		});
		
	</script>

</polymer-element>