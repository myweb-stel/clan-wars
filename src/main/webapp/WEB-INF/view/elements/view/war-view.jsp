<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-button/paper-button.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-dialog/paper-dialog.html">

<%@ include file="war-create-view.jsp" %>
<%@ include file="../model/war-model.jsp" %>

<polymer-element 
	name="war-view"
	attributes="">

	<template>
		
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<div vertical layout>
		
			<template if="{{ error }}">
				<div id="error" horizontal center-justified layout class="error">
					{{ error }}
				</div>
			</template>
			
			<div horizontal center-justified layout>
				
				<paper-button raised on-tap="{{ toggleCreate }}">
					<spring:message code="war.create" text="Create" />
				</paper-button>
				
				<paper-button raised on-tap="{{ goCurrent }}">
					<spring:message code="war.current" text="Current" />
				</paper-button>
			
			</div>
			
			<div horizontal center-justified layout>
				
				<div>
					<template repeat="{{ war in wars }}">
					
						<core-item
							label="{{ war.startDate }}">
							<paper-button>
								View
							</paper-button>
						</core-item>
						
					</template>
				</div>
				
			</div>
			
		</div>
		
		<paper-dialog id="dialogCreate" heading=''>
			<war-create-view></war-create-view>
		</paper-dialog>
		
		<core-ajax
			id="getWars"
			method="GET"
			url="${pageContext.request.contextPath}/json/wars"
			handleAs="json"
			response="{{ wars }}"
			params='{
					"idClan":"{{ idClan }}",
					"csrf":"${_csrf.token}"
				}'>
		</core-ajax>
		
	</template>

	<script>

		Polymer('war-view', {
			
			ready: function() {
				this.error = '${error}';	
				this.$.getWars.go();
			},
			
			toggleCreate: function() {
				this.$.dialogCreate.opened = true;
			},
			
			goCurrent: function() {
				window.location.href = "${pageContext.request.contextPath}" + "/war/current";
			}
			
		});
		
	</script>

</polymer-element>