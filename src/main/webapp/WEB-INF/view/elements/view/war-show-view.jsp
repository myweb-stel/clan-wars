<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-progress/paper-progress.html">

<%@ include file="../model/war-model.jsp"%>

<polymer-element 
	name="war-show-view"
	attributes="">

	<template>
		
		<template if="{{ loading }}">
			<div horizontal center-justified layout>
				<paper-progress indeterminate></paper-progress>
			</div>
		</template>
		
		<template if='{{!loading}}'>
			<war-model 
				war="{{ war }}"
				csrf="${_csrf.token}">
			</war-model>
		</template>
		
		<core-ajax
			auto
			id="getWar"
			url='<c:url value="${url}"></c:url>'
			handleAs="json"
			loading="{{ loading }}"
			response="{{ war }}">
		</core-ajax>
		
	</template>

	<script>

		Polymer('war-show-view', {
			
			ready: function() {
				
			},
			
		});
		
	</script>

</polymer-element>