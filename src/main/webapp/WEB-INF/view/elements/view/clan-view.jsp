<%@ include file="clan-find-view.jsp"%>

<polymer-element
	name="clan-view"
	attributes="csrf">

	<template>
	
		<div horizontal center-justified layout>
			${error}
		</div>
		
		<div horizontal around-justified layout>
			<div>
				<paper-button
					raised
					on-tap="{{ showFindClan }}">
					<core-icon id="arrowFind" icon="arrow-drop-down"></core-icon>
					Find Clan 
				</paper-button>
			</div>
			
			<div>	
				<paper-button
					raised
					on-tap="{{ showCreateClan }}">
					<core-icon icon="flag"></core-icon>
					Create Clan
				</paper-button>
			</div>
		</div>
		
		<template if="{{ find }}">
			<clan-find-view
				csrf="{{ csrf }}">
			</clan-find-view>
		</template>
		
		<paper-action-dialog id="dialogCreateClan" heading="Make a new clan">
		
			<form id="formCreateClan" method="POST" action="${pageContext.request.contextPath}/clan/create">
				<paper-input-decorator
					floatingLabel
					label="Clan name">
					<input
						value="{{ clanName }}" />
				</paper-input-decorator>
				
				<input
					type="hidden"
					name="name"
					value="{{ clanName }}" />
				<input 
					type="hidden" 
					name="_csrf" 
					value="{{ csrf }}" />
			</form>
			
			<paper-button >
				Decline
			</paper-button>
			<paper-button autofocus on-tap="{{ createClan }}">
				Accept
			</paper-button>
			
		</paper-action-dialog>
		
	</template>

	<script>

		Polymer('clan-view', {
			
			ready: function() {
				
			},
			
			showFindClan: function() {
				if(this.find) {
					this.$.arrowFind.icon = "arrow-drop-down";
					this.find = false;
				} else {
					this.$.arrowFind.icon = "arrow-drop-up";
					this.find = true;
				}
			},
			
			showCreateClan: function() {
				this.$.dialogCreateClan.opened = true;
			},
			
			createClan: function() {
				this.$.formCreateClan.submit();
			}
			
						
		});
		
	</script>

</polymer-element>