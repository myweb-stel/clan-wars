<link rel="import" href="${pageContext.request.contextPath}/web/components/core-ajax/core-ajax.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/core-selector/core-selector.html">

<polymer-element name="war-create-view">

	<template>
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<c:url var="urlMakeWar" value="/war/new"></c:url>
		<form
			id="makeWar"
			method="POST" 
			action="${urlMakeWar}">
		
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			
			<div vertical around-justified layout wrap>
				
				<div>
					<core-selector
						id="selectNames"
						class="select-war-create"
						multi>
						<template repeat="{{ clan.members as player }}">
							<div name="{{ player.member.username }}">
								{{ player.member.username }}
							</div>
						</template>
					</core-selector>
				</div>
				
				<div>
					<select name="name" multiple>
						<template repeat="{{ $.selectNames.selected as player }}">
							<option selected value="{{ player }}">
								{{ player }}
							</option>
						</template>
					</select>
				</div>
				
				<div>
					<paper-button on-tap="{{ makeWar }}" raised>
						<spring:message code="war_create.create" text="Create war" />
					</paper-button>
				</div>
			
			</div>
			
		</form>
		
		<core-ajax
			id="getClan"
			url="${pageContext.request.contextPath}/json/clan?player=${username}"
			handleAs="json"
			response="{{ clan }}">
		</core-ajax>
		
	</template>

	<script>

		Polymer('war-create-view', {
			
			ready: function() {
				this.$.getClan.go();
			},
			
			makeWar: function() {
				this.$.makeWar.submit();
			},
			
		});
		
	</script>

</polymer-element>