<%@ include file="../model/clan-model.jsp" %>

<polymer-element 
	name="profile-view"
	attributes="">

	<template>
		
		<div vertical center-justified layout>
			
			<div vertical layout>
			
				<div horizontal layout>
					
						<div flex>
							<core-item
								icon="account-circle"
								label="${username}">
							</core-item>
						</div>
					
				</div>
				
				<div>
					<template if="{{ clan != '' }}">
						
						<clan-model
							clan="{{ clan }}"
							member="true"
							csrf="${_csrf.token}"></clan-model>
						
					</template>
				</div>
				
			</div>
			
		</div>
		
		<core-ajax
			id="getClan"
			url="${pageContext.request.contextPath}/json/clan?player=${username}"
			handleAs="json"
			response="{{ clan }}">
		</core-ajax>
		
	</template>

	<script>

		Polymer('profile-view', {
			
			ready: function() {
				this.$.getClan.go();
			},
			
		});
		
	</script>

</polymer-element>