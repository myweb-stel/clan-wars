<link rel="import" href="${pageContext.request.contextPath}/web/components/core-ajax/core-ajax.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-button/paper-button.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-dialog/paper-action-dialog.html">

<%@ include file="../model/clan-member-model.jsp" %>
<%@ include file="../model/clan-request-model.jsp" %>

<polymer-element 
	name="clan-model"
	attributes="clan member">

	<template>
	
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<div horizontal center-justified layout wrap class="box-clan">
					
			<div>
				<core-item
					icon="home"
					label="Clan: {{ clan.name }}">
				</core-item>
			</div>
			
			<div horizontal layout>
				<div>
					<template if="{{ !member }}">
						<paper-button raised on-tap="{{ joinClan }}">
							<spring:message code="clan.join" text="Join" />
						</paper-button>
					</template>
					
					<template if="{{ member }}">
						<paper-button raised on-tap="{{ leaveClan }}">
							<spring:message code="clan.leave" text="Leave" />
						</paper-button>
					</template>
				</div>
				
				<div>
					<paper-button
						raised
						on-tap="{{ viewDetail }}">
						<spring:message code="clan.view" text="View" />
					</paper-button>
				</div>
			</div>
					
		</div>
		
		<paper-action-dialog 
			id="dialogShowClan"
			heading="{{ clan.name }}">
			<div vertical layout>
				
				<div horizontal layout>
					
					<!-- TODO revistar por que aqui no coge el css -->
					<div style="width: 240px;">
						<!-- Deberia ir el label 'Nombre' -->
					</div>
					<div>
						<spring:message code="clan.war_number" text="War number" />
					</div>
					
				</div>
				
				<template if="{{ allowEdit == 'true' }}">
					<template repeat="{{ request in clan.requests }}">
						<clan-request-model
							idClan="{{ clan.id }}"
							request="{{ request }}"
							csrf="${_csrf.token}">
						</clan-request-model>
					</template>
				</template>
				
				<template repeat="{{ clanMember in clan.members }}">
					<clan-member-model
						idClan="{{ clan.id }}"
						clanMember="{{ clanMember }}"
						allowEdit="{{ allowEdit }}">
					</clan-member-model>
				</template>
				
			</div>
			<paper-button affirmative>
				Close
			</paper-button>
		</paper-action-dialog>
		
		<form 
			id="joinClan" 
			method="POST" 
			action="${pageContext.request.contextPath}/clan/request">
			
			<input type="hidden" name="clan.id" value="{{ clan.id }}">
			<input type="hidden" name="_csrf" value="${_csrf.token}" />
		</form>
		
		<form 
			id="leaveClan" 
			method="POST" 
			action="${pageContext.request.contextPath}/clan/leave">
			
			<input type="hidden" name="_csrf" value="${_csrf.token}" />
		</form>
		
		<core-ajax
			id="allowEdit"
			method="POST"
			url="${pageContext.request.contextPath}/clan/member/allowedit"
			handleAs="text"
			response="{{ allowEdit }}"
			params='{
					"_csrf":"${_csrf.token}"
				}'>
		</core-ajax>
		
	</template>

	<script>

		Polymer('clan-model', {
			
			ready: function() {
				this.$.allowEdit.go();
			},
			
			joinClan: function() {
				this.$.joinClan.submit();
			},
			
			leaveClan: function() {
				this.$.leaveClan.submit();
			},
			
			viewDetail: function() {
				this.$.dialogShowClan.opened = true;
			}
			
		});
		
	</script>

</polymer-element>