<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-checkbox/paper-checkbox.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-button/paper-button.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/core-collapse/core-collapse.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/core-ajax/core-ajax.html">

<%@ include file="../model/attack-model.jsp" %>

<polymer-element 
	name="war-player-model"
	attributes="war warPlayer csrf">

	<template>
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<div id="warPlayer" vertical layout class="box-war-player">
			<div horizontal layout class="box-war-player-header">
				<div style="margin-right: 12px;">
					<core-item
						icon="label"
						label="{{ warPlayer.warNumber }}">
					</core-item>
				</div>
				<div>
					<core-item
						icon="account-circle"
						label="{{ warPlayer.player.username }}">
					</core-item>
				</div>
			</div>
			<div horizontal arround-justified layout>
				<div vertical layout flex class="box-war-player-item-check">
					<div horizontal center-justified layout>
						<spring:message code="war_player.king" text="KING" />
					</div> 
					<div horizontal center-justified layout>
						<paper-checkbox
							id="haveKing"
							on-tap="{{ updateChecks }}">
						</paper-checkbox>
					</div>
				</div>
				<div vertical layout flex class="box-war-player-item-check">
					<div horizontal center-justified layout>
						<spring:message code="war_player.queen" text="QUEEN" />
					</div> 
					<div horizontal center-justified layout>
						<paper-checkbox
							id="haveQueen"
							on-tap="{{ updateChecks }}">
						</paper-checkbox>
					</div>
				</div>
				<div vertical layout flex class="box-war-player-item-check">
					<div horizontal center-justified layout>
						<spring:message code="war_player.potions" text="POTIONS" />
					</div> 
					<div horizontal center-justified layout>
						<paper-checkbox
							id="havePotions"
							on-tap="{{ updateChecks }}">
						</paper-checkbox>
					</div>
				</div>
			</div>
			
			<div horizontal center-justified layout class="war-player-label">
				<template if="{{ warPlayer.attacks.length > 0 || warPlayer.player.username == username }}">
					<paper-button on-tap="{{ showAttacks }}" raised class="button-raised">
						<template if="{{ $.attacksCollapse.opened }}">
							<spring:message 
								code="war_player.hide_attacks" 
								text="Show attacks" />
						</template>
						
						<template if="{{ !$.attacksCollapse.opened }}">
							<spring:message 
								code="war_player.show_attacks" 
								text="Show attacks" />
						</template>
					</paper-button>
				</template>
			</div>
			
			<div horizontal center-justified layout>
				<template if="{{ warPlayer.attacks.length == 0 && warPlayer.player.username != username }}">
					The player still has not <br />
					defined any attack yet.
				</template>
			</div>
			
			<core-collapse id="attacksCollapse">
			
				<div vertical center-justified layout wrap>
					
					<template repeat="{{ attack in warPlayer.attacks }}">
						<div>
							<attack-model
								war="{{ war }}"
								warPlayer="{{ warPlayer.id }}"
								warAttack="{{ attack }}"
								csrf="{{ csrf }}">
							</attack-model>
						</div>
					</template>
					
					<div>
						<template if="{{ allowNewAttack }}">
							<attack-model
								war="{{ war }}"
								warPlayer="{{ warPlayer.id }}"
								csrf="{{ csrf }}">
							</attack-model>
						</template>
					</div>
					
				</div>
			
			</core-collapse>
			
		</div>
		
		<core-ajax
			id="updateChecks"
			method="POST"
			url="${pageContext.request.contextPath}/war/saveplayer"
			handleAs="text"
			params='{
					"id":"{{ warPlayer.id }}",
					"player.id":"{{ warPlayer.player.id }}",
					"war.id":"{{ war }}",
					"haveKing":"{{ $.haveKing.checked }}", 
					"haveQueen":"{{ $.haveQueen.checked }}", 
					"havePotions":"{{ $.havePotions.checked }}",
					"warNumber":"{{ warPlayer.warNumber }}",
					"_csrf":"{{ csrf }}"
				}'>
		</core-ajax>
		
	</template>

	<script>

		Polymer('war-player-model', {
			
			allowNewAttack: false,
			
			ready: function() {
				this.username = '${username}';
				this.$.haveKing.checked = this.warPlayer.haveKing;
				this.$.haveQueen.checked = this.warPlayer.haveQueen;
				this.$.havePotions.checked = this.warPlayer.havePotions;
				this.codeShowAttacks = "war_player.show_attacks";
				this.checkName();
			},
			
			checkName: function() {
				if (this.username == this.warPlayer.player.username) {
					this.allowNewAttack = true;
				} else {
					this.disableView();
				}
			},
			
			updateChecks: function() {
				this.$.updateChecks.go();
			},
			
			showAttacks: function() {
				this.$.attacksCollapse.toggle();
			},
			
			disableView: function() {
				this.$.haveKing.disabled = true;
				this.$.haveQueen.disabled = true;
				this.$.havePotions.disabled = true;
			}
			
		});
		
	</script>

</polymer-element>