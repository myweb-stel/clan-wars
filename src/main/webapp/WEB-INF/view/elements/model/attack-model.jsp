<link rel="import" href="${pageContext.request.contextPath}/web/components/polymer-ui-ratings/polymer-ui-ratings.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-fab/paper-fab.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-input/paper-input-decorator.html">

<polymer-element 
	name="attack-model"
	attributes="war warPlayer warAttack csrf">
	
	<template>
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />

		<template if="{{ warAttack.attack.enemy != null }}">
			
			<core-item class="attack">
				<div vertical layout>
					<div horizontal layout>
						<div class="label">
							<spring:message code="attack.attack" text="Attack:" />
						</div>
						<div class="value">
							{{ warAttack.typeAttackInWar.value }}
						</div>
					</div>
					
					<div horizontal layout>
						<div class="label">
							<spring:message code="attack.enemy_number" text="# Enemy:" />
						</div>
						<div class="value">
							{{ warAttack.attack.enemy.warNumber }}
						</div>
					</div>
					
					<div horizontal center-justified layout>
						<polymer-ui-ratings
							disabled="true"
							count="3"
							value="{{ warAttack.attack.stars }}">
						</polymer-ui-ratings>
					</div>
					
					<div horizontal layout>
						<div class="label">
							<spring:message code="attack.strategy" text="Strategy:" />
						</div>
						<div class="value">
							{{ warAttack.attack.strategy.value }}
						</div>
					</div>
					
					<div horizontal layout>
						<div class="label">
							<spring:message code="attack.time_range" text="Time range:" />
						</div>
						<div class="value">
							{{ warAttack.attack.timeRange.value }}
						</div>
					</div>
					
					<div horizontal layout>
						<div class="label">
							<spring:message code="attack.clan_castle" text="Clan clastle" />
						</div>
						<div class="value">
							<paper-button
								raised
								on-tap="{{ inDev }}">
								<spring:message code="attack.clan_clastle_view" text="View" />
							</paper-button>
						</div>
					</div>
				</div>
			</core-item>
			
		</template>
		
		<template if="{{ warAttack.attack.enemy == null }}">
			
			<form id="formSaveAttack" method="POST" action="/clanwar/war/saveattack">
				
				<input type="hidden" name="warPlayer.id" value="{{ warPlayer }}" />
				<input type="hidden" name="warPlayer.war.id" value="{{ war }}" />
				
				<input type="hidden" name="attack.enemy.war.id" value="{{ war }}" />
				<input type="hidden" name="attack.stars" value="{{ stars }}" />
				<input type="hidden" name="attack.strategy.id" value="{{ strategy }}" />
				<input type="hidden" name="attack.timeRange.id" value="{{ timeRange }}" />
				
				<input type="hidden" name="typeAttackInWar.id" value="{{ typeAttack }}" />
				
				<input type="hidden" name="_csrf" value="{{ csrf }}" />
				
				<core-item class="attack">
					<div vertical layout>
					
						<div horizontal around-justified layout>
							
							<div vertical center-justified layout>
								<spring:message code="attack.new_attack" text="New attack" />
							</div>
							
							<div horizontal center-justified layout>
								<paper-fab
									class="button-raised"
									icon="done"
									on-tap="{{ saveAttack }}">
								</paper-fab>
							</div>
						</div>
						
						<div horizontal center-justified layout>
						
							<paper-input-decorator 
								floatingLabel
								label="<spring:message code="attack.enemy_number" text="# Enemy" />">
								<input
									is="core-input"
									type="number" 
									name="attack.enemy.warNumber"
									value="{{ warNumber }}"
									on-keypress="{{ isNumber }}"> 
							</paper-input-decorator>
							
						</div>
						
						<div horizontal center-justified layout>
							<polymer-ui-ratings
								disabled="false"
								count="3"
								value="{{ stars }}">
							</polymer-ui-ratings>
						</div>
						
						<div horizontal center-justified layout>
							<core-selector class="select-attack" selected="{{ typeAttack }}">
								<template repeat="{{ typeAttackEnum in enums.typesAttacks }}">
									<div name="{{ typeAttackEnum.id }}">
										{{ typeAttackEnum.value }}
									</div>
								</template>
							</core-selector>
						</div>
						
						<div horizontal center-justified layout>
							<core-selector class="select-attack" selected="{{ strategy }}">
								<template repeat="{{ strategyEnum in enums.strategies }}">
									<div name="{{ strategyEnum.id }}">
										{{ strategyEnum.value }}
									</div>
								</template>
							</core-selector>
						</div>
						
						<div horizontal center-justified layout>
							<core-selector class="select-attack" selected="{{ timeRange }}">
								<template repeat="{{ timeRamgeEnum in enums.timesRanges }}">
									<div name="{{ timeRamgeEnum.id }}">
										{{ timeRamgeEnum.value }}
									</div>
								</template>
							</core-selector>
						</div>
						
						<div horizontal center-justified layout>
							<paper-button
								raised
								class="button-raised"
								on-tap="{{ inDev }}">
								<spring:message code="attack.clan_castle" text="Clan clastle" />
							</paper-button>
						</div>
						
					</div>
				</core-item>
				
			</form>	
			
		</template>
		
		<core-ajax
			id="getEnums"
			url="/clanwar/json/enums"
			handleAs="json"
			response="{{ enums }}">
		</core-ajax>
		
	</template>
	
	<script>		

		Polymer('attack-model', {
			
			ready: function() {
				this.$.getEnums.go();
			},
			
			inDev: function () {
				alert('Funcion en desarrollo.');
			},
			
			isNumber: function(event, detail, target) {
			    var charCode = event.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			    	event.preventDefault();
			    }
			},
			
			saveAttack: function() {
				this.$.formSaveAttack.submit();
			}
			
		});
		
	</script>

</polymer-element>