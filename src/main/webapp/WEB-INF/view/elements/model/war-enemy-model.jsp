<polymer-element 
	name="war-enemy-model"
	attributes="war warEnemy warPlayers csrf">

	<template>
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<div vertical layout class="box-war-enemy">
		
			<div horizontal layout class="box-war-enemy-header">
				<div>
					<core-item
						icon="label"
						label="{{ warEnemy.warNumber }}">
					</core-item>
				</div>
			</div>
			
			<div id="labelShow" class="war-enemy-label" vertical center-justified layout>
				{{ numberAttacks }} <spring:message code="war_enemy.attacks" text="attack/s" />
			</div>
			
			<div id="buttonShow" horizontal center-justified layout hidden style="margin-top: 10px;">
				<paper-button on-tap="{{ showAttacks }}" raised class="button-raised-red">
					<template if="{{ $.attacksCollapse.opened }}">
						<spring:message 
							code="war_enemy.hide_attacks" 
							text="Show attacks" />
					</template>
					
					<template if="{{ !$.attacksCollapse.opened }}">
						<spring:message 
							code="war_enemy.show_attacks" 
							text="Show attacks" />
					</template>
				</paper-button>
			</div>
			
			<core-collapse id="attacksCollapse">
				<div horizontal center-justified layout wrap>
					<template repeat="{{ warPlayers as warPlayer }}">
					
						<template repeat="{{ warAttack in warPlayer.attacks }}">
							
							<template if="{{ warAttack.attack.enemy.id == warEnemy.id }}">
								
								<div vertical center-justified layout>
									<div horizontal center-justified layout>
										<core-item
											label="{{ warPlayer.player.username }}">
										</core-item>
									</div>
									<div>
										<attack-model
											war="{{ war }}"
											warPlayer="{{ warPlayer.id }}"
											warAttack="{{ warAttack }}"
											csrf="{{ csrf }}">
										</attack-model>
									</div>
								</div>
								
							</template>
						
						</template>
						
					</template>
				</div>
			</core-collapse>
		
		</div>
		
	</template>

	<script>

		Polymer('war-enemy-model', {
			
			ready: function() {
				this.numberAttacks = "Haven't";
				this.haveAttacks();
			},
			
			haveAttacks: function() {
				for (var i = 0; i < this.warPlayers.length; i++) {
					if (this.warPlayers[i].attacks.length > 0) {
						for (var j = 0; j < this.warPlayers[i].attacks.length; j++) {
							if (this.warPlayers[i].attacks[j].attack.enemy.id == this.warEnemy.id) {
								this.$.buttonShow.hidden = false;
								this.numberAttacks = this.checkNumberAttacks();
								break;
							}
						}
					}
				}
			},
			
			checkNumberAttacks: function() {
				var number = 0;
				for (var i = 0; i < this.warPlayers.length; i++) {
					if (this.warPlayers[i].attacks.length > 0) {
						for (var j = 0; j < this.warPlayers[i].attacks.length; j++) {
							if (this.warPlayers[i].attacks[j].attack.enemy.id == this.warEnemy.id) {
								number++;	
							}
						}
					}
				}
				return number;
			},
			
			showAttacks: function() {
				this.$.attacksCollapse.toggle();
				this.$.playerNames.hidden = !this.$.playerNames.hidden;
			}
			
		});
		
	</script>

</polymer-element>