<link rel="import" href="${pageContext.request.contextPath}/web/components/core-collapse/core-collapse.html">
<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-button/paper-button.html">

<%@ include file="../model/war-player-model.jsp" %>
<%@ include file="../model/war-enemy-model.jsp" %>

<polymer-element 
	name="war-model"
	attributes="war csrf">

	<template>
		
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<div horizontal around-justified layout wrap>
			<div>
				<paper-button
					class="button-raised"
					raised
					on-tap="{{ togglePlayers }}">
					<core-item
						icon="{{ icTogglePlayers }}"
						label="<spring:message code="war.view_players" text="View players" />">
					</core-item>
				</paper-button>
			</div>
			
			<div>
				<paper-button
					class="button-raised"
					raised
					on-tap="{{ toggleEnemies }}">
					<core-item
						icon="{{ icToggleEnemies }}"
						label="<spring:message code="war.view_enemies" text="View enemies" />">
					</core-item>
				</paper-button>
			</div>
		</div>
		
		<div horizontal center-justified layout wrap>
			
			<div horizontal center-justified layout>
				<core-collapse id="players">
					<template repeat="{{ warPlayer in war.players }}">
					
						<div>
							<war-player-model
								war="{{ war.id }}"
								warPlayer="{{ warPlayer }}"
								csrf="{{ csrf }}">
							</war-player-model>
						</div>
					
					</template>
				</core-collapse>
			</div>
			
			
			<div horizontal center-justified layout>
				<core-collapse id="enemies">
					<template repeat="{{ warEnemy in war.enemies }}">
						
						<div>
							<war-enemy-model
								war="{{ war.id }}"
								warEnemy="{{ warEnemy }}"
								warPlayers="{{ war.players }}"
								csrf="{{ csrf }}">
							</war-enemy-model>
						</div>
					
					</template>
				</core-collapse>
			</div>
			
		</div>
		
	</template>

	<script>

		Polymer('war-model', {
			
			ready: function() {
				this.icTogglePlayers = "expand-more";
				this.icToggleEnemies = "expand-more";
			},
			
			togglePlayers: function() {
				this.$.players.toggle();
				if (this.$.players.opened) {
					this.icTogglePlayers = "expand-less";
				} else {
					this.icTogglePlayers = "expand-more";
				}
			},
			
			toggleEnemies: function() {
				this.$.enemies.toggle();
				if (this.$.enemies.opened) {
					this.icToggleEnemies = "expand-less";
				} else {
					this.icToggleEnemies = "expand-more";
				}
			}
			
		});
		
	</script>

</polymer-element>