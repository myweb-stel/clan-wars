<link rel="import" href="${pageContext.request.contextPath}/web/components/paper-input/paper-input-decorator.html">

<polymer-element 
	name="clan-member-model"
	attributes="idClan clanMember allowEdit">

	<template>
		
		<link rel="stylesheet" type="text/css" href="<c:url value="/web/css/master.css"></c:url>" />
		
		<div horizontal start-justified layout>
			
			<div vertical around-justified layout class="box-clan-player-name">
				<div>
					<core-item
						icon="verified-user"
						label="{{ clanMember.member.username }}">
					</core-item>
				</div>
			</div>
			
			<div>
				<paper-input-decorator
					id="inputDecoratorWarNumber"
					disabled
					label="War number:">
					<input
						id="inputWarNumber"
						is="core-input"
						disabled
						value="{{ warNumber }}"                                      
						committedValue="{{ warNumber }}" />
				</paper-input-decorator>
			</div>
		</div>
		
		<core-ajax
			id="updateWarNumber"
			method="POST"
			url="${pageContext.request.contextPath}/clan/member/update"
			handleAs="text"
			params='{
					"member.id":"{{ clanMember.member.id }}",
					"warNumber":"{{ warNumber }}",
					"_csrf":"${_csrf.token}"
				}'>
		</core-ajax>
		
	</template>

	<script>

		Polymer('clan-member-model', {
			
			ready: function() {
				this.warNumber = this.clanMember.warNumber;
				//this.checkRole();
			},
			
			allowEditChanged: function() {
				if (this.allowEdit == 'true') {
					this.$.inputDecoratorWarNumber.disabled = false;
					this.$.inputWarNumber.disabled = false;
				}
			},
			
			warNumberChanged: function() {
				if(this.warNumber != ''
						&& this.warNumber != this.clanMember.warNumber) {
					this.$.updateWarNumber.go();
				}
			}
			
		});
		
	</script>

</polymer-element>