<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>
	<head>
		<title>SignUp</title>
	</head>
	
	<body>
		
		${error}
		
		<c:url var="urlSignUp" value="/signup"></c:url>
		<form:form method="POST" commandName="player" action="${urlSignUp}">
		
			Username: <input name="username"> <br />
			Password: <input name="password"> <br />
			
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			
			<input type="submit">
		
		</form:form>
			
	</body>
</html>