<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
	<head>
		<script src="${pageContext.request.contextPath}/web/components/webcomponentsjs/webcomponents.js"></script>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1, user-scalable=yes">
		
		<title>
			<spring:message code="home.title" text="Home" />
		</title>
	</head>
	
	<body>
		<%@ include file="elements/layouts/layout-main.jsp"%>
		
		<layout-main
			logged="true"
			token="${_csrf.token}"
			path="${pageContext.request.contextPath}">
			
			HOME NOW IS EMPTY
			
		</layout-main>
		
	</body>
</html>