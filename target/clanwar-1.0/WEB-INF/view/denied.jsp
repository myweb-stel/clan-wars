<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1, user-scalable=yes">
        
		<link rel="import" href='web/layouts/layout-main.html'>
		
		<title>
			<spring:message code="denied.title" text="Acces denied" />
		</title>
	</head>
	
	<body>
		
		<layout-main
			path="${pageContext.request.contextPath}">
			
			<spring:message code="denied.title" text="Acces denied" />
			
		</layout-main>
			
	</body>
</html>