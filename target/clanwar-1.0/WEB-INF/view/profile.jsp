<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1, user-scalable=yes">
		
		<link rel="import" href='<c:url value="/web/layouts/layout-main.html"></c:url>'>
		<link rel="import" href='<c:url value="/web/elements/model/clan-model.html"></c:url>'>
		<link rel="import" href='<c:url value="/web/components/core-ajax/core-ajax.html"></c:url>'>
			
		<title>
			<spring:message code="profile.title" text="Profile" />
		</title>
	</head>
	
	<body>
	
		<%@ include file="elements/view/profile-view.jsp" %>
		
		<layout-main
			selectedMenu="0"
			logged="true"
			token="${_csrf.token}"
			path="${pageContext.request.contextPath}">
			
			<profile-view></profile-view>
			
		</layout-main>
		
	</body>
</html>