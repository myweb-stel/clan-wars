<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1, user-scalable=yes">
        
		<link rel="import" href='web/layouts/layout-main.html'>
		
		<title>
			<spring:message code="login.title" text="Login" />
		</title>
	</head>
	
	<body>
		
		<layout-main
			selectedMenu="3"
			path="${pageContext.request.contextPath}">
			
			<form-login></form-login>
			
		</layout-main>
			
	</body>
</html>

<polymer-element name="form-login" on-keypress="{{ loginEnter }}">

	<template>
		
		<div horizontal center-justified layout>
			${error}
		</div>
		
		<div horizontal center-justified layout>
			
			<c:url var="urlLogin" value="/login"></c:url>
			<form id="formLogin" method="POST" action="${urlLogin}">
			
				<input 
					type="hidden" 
					name="username"
					value="{{ username }}"> 
				<paper-input
					id="piUsername" 
					floatingLabel
					label="Username:"
					value="{{ username }}"></paper-input> <br />
					
				<input 
					type="hidden" 
					name="password"
					value="{{ password }}"> 
				<paper-input 
					floatingLabel
					label="Password:"
					value="{{ password }}"></paper-input> <br />
				
				<input 
					type="hidden" 
					name="${_csrf.parameterName}" 
					value="${_csrf.token}" />
				
				<div horizontal center-justified layout>
					<paper-button
						raised
						on-tap="{{ login }}">Entrar</paper-button>
				</div>
			
			</form>
		</div>
		
	</template>

	<script>

		Polymer('form-login', {
			
			ready: function() {
				this.$.piUsername.focus();
			},
			
			login: function() {
				this.$.formLogin.submit();
			},
			
			loginEnter: function(event, detail, sender) {
				if(event.keyCode == 13) {
					this.login();
				}
			}
						
		});
		
	</script>

</polymer-element>